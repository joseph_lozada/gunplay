﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoverBody : Body
{
    public float m_movementSpeed;

	// Use this for initialization
	void Start ()
    {
        rb = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    void Hover()
    {
        rb.velocity = Vector3.up * m_movementSpeed;
        //Debug.Log(rb.gameObject.name);
    }

    public override void AttachController(Brain a_brain)
    {
        if (a_brain == null)
        {
            enabled = false;
        }
        else
        {
            m_brain = a_brain;
            a_brain.m_cameraCon.m_target = transform;

            a_brain.m_command[(int)Brain.Command.Forward]   += delegate { Move(transform.forward, m_movementSpeed); };
            a_brain.m_command[(int)Brain.Command.Backward]  += delegate { Move(-transform.forward, m_movementSpeed); };
            a_brain.m_command[(int)Brain.Command.Left]      += delegate { Move(-transform.right, m_movementSpeed); };
            a_brain.m_command[(int)Brain.Command.Right]     += delegate { Move(transform.right, m_movementSpeed); };
            a_brain.m_command[(int)Brain.Command.LClick]    += null;
            a_brain.m_command[(int)Brain.Command.RClick]    += null;
            a_brain.m_command[(int)Brain.Command.Jump]      += Hover;
        }
    }
}
