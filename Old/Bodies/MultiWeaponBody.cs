﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultiWeaponBody : Body
{
    public float m_movementSpeed = 5;
    public Weapon[] weapons;

	// Use this for initialization
	void Start ()
    {
        rb = GetComponent<Rigidbody>();

        for (int i = 0; i < weapons.Length; i++)
        {
            weapons[i].m_camera = m_brain.m_cameraCon.m_camera;
        }
	}
	
	// Update is called once per frame
	void Update ()
    {
        foreach (Weapon w in weapons)
        {
            w.WeaponUpdate();
        }

        if (Input.GetKey(KeyCode.W))
        {
            Move(transform.forward, m_movementSpeed);
        }
        if (Input.GetKey(KeyCode.A))
        {
            Move(-transform.right, m_movementSpeed);
        }
        if (Input.GetKey(KeyCode.S))
        {
            Move(-transform.forward, m_movementSpeed);
        }
        if (Input.GetKey(KeyCode.D))
        {
            Move(transform.right, m_movementSpeed);
        }
        if (Input.GetMouseButton(0))
        {
            foreach (Weapon w in weapons)
            {
                w.Attack();
            }
        }
    }

    public override void AttachController(Brain a_brain)
    {
        throw new NotImplementedException();
    }
}
