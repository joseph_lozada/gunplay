﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBody : Body
{
    public float m_movementSpeed = 10;
    public float m_jumpHeight = 10;
    public float m_gravity = 10;
    public Weapon m_Weapon;

	// Use this for initialization
	void Start ()
    {
        rb = GetComponent<Rigidbody>();
        AttachController(m_brain);

        if (m_Weapon != null && m_brain != null)
        {
            m_Weapon.m_wielder = this;
            m_Weapon.m_camera = m_brain.m_cameraCon.GetComponent<Camera>();
        }
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (m_Weapon != null)
        {
            m_Weapon.WeaponUpdate();
        }

        HandleRotation();
	}

    private void FixedUpdate()
    {
        rb.velocity += -Vector3.up * m_gravity * Time.fixedDeltaTime;
    }

    void Jump()
    {
        RaycastHit info;
        Physics.Raycast(transform.position - Vector3.up * 0.5f, -Vector3.up, out info, 1);

        Debug.DrawRay(transform.position - Vector3.up * 0.5f, -Vector3.up, Color.blue, 1);
        if (info.collider != null)
        {
            //Debug.Log(info.collider.name);
            rb.velocity += Vector3.up * m_jumpHeight;
        }
    }

    public override void AttachController(Brain a_brain)
    {
        if (a_brain == null)
        {
            enabled = false;
        }
        else
        {
            m_brain.m_cameraCon.m_target = transform;

            a_brain.m_command[(int)Brain.Command.Forward]   += delegate { Move(transform.forward,m_movementSpeed); };
            a_brain.m_command[(int)Brain.Command.Backward]  += delegate { Move(-transform.forward, m_movementSpeed); };
            a_brain.m_command[(int)Brain.Command.Left]      += delegate { Move(-transform.right, m_movementSpeed); };
            a_brain.m_command[(int)Brain.Command.Right]     += delegate { Move(transform.right, m_movementSpeed); };
            a_brain.m_command[(int)Brain.Command.LClick]    += m_Weapon.Attack;
            a_brain.m_command[(int)Brain.Command.RClick]    += m_Weapon.AltAttack;
            a_brain.m_command[(int)Brain.Command.Jump]      += Jump;
        }
    }


    private void HandleRotation()
    {
        //Vector2 xz = new Vector2(m_brain.m_camera.transform.forward.x, m_brain.m_camera.transform.forward.z);
        //transform.Rotate(Vector3.up, -Vector3.SignedAngle(xz, new Vector2(transform.forward.x, transform.forward.z), Vector3.up));
        Vector2 y = new Vector3(transform.eulerAngles.x, m_brain.m_cameraCon.transform.forward.y, transform.eulerAngles.z);
        transform.eulerAngles = y;
    }
}
