﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public abstract class Body : MonoBehaviour
{
    public Brain m_brain;
    public Rigidbody rb;

    // Use this for initialization
    void Start ()
    {
        rb = GetComponent<Rigidbody>();
        AttachController(m_brain);
	}
	
	// Update is called once per frame
	void Update ()
    {

    }

    protected void Move(Vector3 a_direction, float a_movementSpeed)
    {
        rb.velocity += a_direction;
        Vector3 velocityXZ = new Vector3(rb.velocity.x, 0, rb.velocity.z);
        if (velocityXZ.magnitude > a_movementSpeed)
        {
            velocityXZ = velocityXZ.normalized * a_movementSpeed;
            velocityXZ.y = rb.velocity.y;
            rb.velocity = velocityXZ;
        }
    }

    public abstract void AttachController(Brain a_brain);
}
