﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class CameraController : MonoBehaviour
{
    public Camera m_camera;
    public Transform m_target;
    public Vector3 m_offset;
    public Vector2 m_mouseSensitivity = new Vector2(1, 1);

    // Use this for initialization
    void Start ()
    {
        m_camera = GetComponent<Camera>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        HandleMovement();
		HandleRotation();
	}

    void HandleRotation()
    {
        //Vector3 rotation = new Vector3(0, Input.GetAxis("Mouse X") * m_mouseSensitivity.x, 0);
        //m_offset = m_offset * m_target.transform.position;
        //transform.RotateAround(m_target.position,Vector3.up, Input.GetAxis("Mouse X") * m_mouseSensitivity.x);

        m_target.Rotate(Vector3.up, Input.GetAxis("Mouse X") * m_mouseSensitivity.x);
        float Xrotation = -Input.GetAxis("Mouse Y") * m_mouseSensitivity.y;
        Xrotation += transform.eulerAngles.x;

        if (Xrotation > 0 && Xrotation < 90)
        {
            Xrotation = Mathf.Min(Xrotation, 90);
        }
        else if (Xrotation > -90 && Xrotation < 0)
        {
            Xrotation = Mathf.Max(-90, Xrotation);
        }

        transform.localEulerAngles = new Vector3(Xrotation, transform.localEulerAngles.y, 0);
        //Debug.Log(Input.GetAxis("Mouse X"));
    }

    void HandleMovement()
    {
        transform.position += (m_offset + m_target.position - transform.position) * Time.deltaTime * 5;
    }
}
