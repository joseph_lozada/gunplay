﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HackWeapon : Weapon
{

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public override void WeaponUpdate()
    {
        base.WeaponUpdate();
    }

    public override void Attack()
    {
        if (m_cooldownTimer <= 0)
        {
            m_cooldownTimer = m_cooldown;

            RaycastHit info;
            Physics.Raycast(m_camera.transform.position, m_camera.transform.forward, out info);
            if (info.collider != null)
            {
                Debug.DrawLine(m_camera.transform.position, info.point);
                Body bodyTemp = info.collider.GetComponent<Body>();
                if (bodyTemp != null)
                {
                    for (int i = 0; i < m_wielder.m_brain.m_command.Length; i++)
                    {
                        m_wielder.m_brain.m_command[i] = null;
                    }
                    m_wielder.enabled = false;
                    bodyTemp.AttachController(m_wielder.m_brain);

                    m_camera.transform.parent = bodyTemp.transform;
                    m_camera.transform.localPosition = new Vector3(0, 5, 0); 
                }
            }
            else
            {
                Debug.DrawRay(m_camera.transform.position, m_camera.transform.forward);
            }
        }
    }

    public override void AltAttack()
    {
        //Debug.Log(":/");
    }
}
