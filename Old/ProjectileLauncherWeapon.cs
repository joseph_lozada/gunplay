﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileLauncherWeapon : Weapon
{
    public Projectile m_projectile;
    public Transform m_muzzle;
    public float m_inaccuracy;
    public int m_damage;
    public float m_force;
    public float m_projectileSpeed;

    // Use this for initialization
    void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    //public override void OnWeaponFire()
    //{
    //    if (m_cooldownTimer <= 0)
    //    {
    //        m_cooldownTimer = m_cooldown;

    //        float trueDeviancy = 0.5f * m_inaccuracy;
    //        Vector3 inaccuracy = new Vector3
    //        (
    //            UnityEngine.Random.Range(-trueDeviancy, trueDeviancy),
    //            UnityEngine.Random.Range(-trueDeviancy, trueDeviancy),
    //            UnityEngine.Random.Range(-trueDeviancy, trueDeviancy)
    //        );

    //        Vector3 point;
    //        RaycastHit info;
    //        point = transform.position + transform.forward * 100;
    //        point += inaccuracy;
    //        m_muzzle.transform.LookAt(point);

    //        Projectile p;
    //        p = Instantiate(m_projectile);
    //        p.transform.position = m_muzzle.position;
    //        p.m_rb.velocity = m_muzzle.transform.forward * m_projectileSpeed;
    //        p.m_damage = m_damage;
    //        p.transform.forward = m_muzzle.forward;
    //    }
    //}
}
