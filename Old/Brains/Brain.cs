﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Brain : MonoBehaviour
{
    public enum Command
    {
        Forward,
        Backward,
        Left,
        Right,
        Jump,
        LClick,
        RClick,
        ENUM_END
    }

    public delegate void ButtonPress();
    public ButtonPress[] m_command;
    public CameraController m_cameraCon;

    private void Start()
    {
    }

    protected void TryCommand(Command a_command)
    {
        int iC = (int)a_command;
        if (m_command[iC] != null)
        {
            m_command[iC].Invoke();
        }
    }
}
