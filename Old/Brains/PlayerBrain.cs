﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBrain : Brain
{
    KeyCode[] keyBinds;

    // Use this for initialization
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        m_command = new ButtonPress[(int)Command.ENUM_END];
        //if (m_cameraCon == null)
        //{
        //    m_cameraCon = Camera.main;
        //}
        keyBinds = new KeyCode[(int)Command.ENUM_END]
            {
                KeyCode.W,
                KeyCode.S,
                KeyCode.A,
                KeyCode.D,
                KeyCode.Space,
                KeyCode.Mouse0,
                KeyCode.Mouse1,
            };
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.L))
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }

        HandleCommands();
    }

    private void HandleCommands()
    {
        //int iC = (int)Command.ENUM_END;
        for (Command c = 0; c < Command.LClick; c++)
        {
            if (Input.GetKey(keyBinds[(int)c]))
            {
                TryCommand(c);
            }
        }
        for (Command c = Command.LClick; c < Command.ENUM_END; c++)
        {
            if (Input.GetKey(keyBinds[(int)c]))
            {
                TryCommand(c);
            }
        }
    }

}
