﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastWeapon : Weapon
{

    //[Header("Graphics")]


    //private Color[] m_colors;

	// Use this for initialization
	void Start ()
    {
        Initialize();

        m_mask = ~LayerMask.GetMask("Player", "IgnoreGameplay", "CheckOnlyPlayer");
    }
	
	// Update is called once per frame
	public override void CooldownHandler()
    {
        m_cooldownTimer = Mathf.Max(0, m_cooldownTimer - Time.deltaTime);
        LineRendererCooldowns();
        HUDManager.SetCrossHairScale(m_bulletSpread * 0.5f);
        StabilizeWeapon();
    }

    public override void OnWeaponFire()
    {

    }


}