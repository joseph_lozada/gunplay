﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileFireEffect : FireEffect
{
    public Projectile m_projectile;
    public float m_velocity;

	// Use this for initialization
	void Start ()
    {
        enabled = false;
        Initialize();
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    protected override void HandleGraphics()
    {
        throw new NotImplementedException();
    }

    //!!!PROTO: no inaccuracy
    public override void OnFire()
    {
        Projectile p;

        for (int i = 0; i < m_amount; i++)
        {
            p = Instantiate(m_projectile.gameObject).GetComponent<Projectile>();
            p.m_damage = m_damage;
            p.m_force = m_force;
            //p.m_mask = m_mask;
            p.transform.position = m_muzzles[0].position;
            p.transform.forward = m_muzzles[0].forward;
            p.m_prevPos = m_muzzles[0].position;
            p.m_rb.AddForce(m_muzzles[0].forward * m_velocity, ForceMode.VelocityChange);
        }

        m_weapon.m_accuracy = Mathf.Min(m_weapon.m_maxAimDeviancy, m_weapon.m_accuracy + m_recoil);
    }

    public override void Bind(Weapon a_weapon)
    {
    }

    public override void Unbind()
    {
    }
}
