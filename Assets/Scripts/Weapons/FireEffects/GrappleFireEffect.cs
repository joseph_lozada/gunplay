﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//!!!NOTES: anchors don't move with target's rotation
public class GrappleFireEffect : FireEffect
{
    //public float m_detachTime;
    //public float m_detachTimer;
    public Rigidbody m_userRb;                  //!!!BAD: needs to be set manually
    //public int m_amount;
    public float m_rendererActiveTime;
    public GameObject m_lineRendererRef;        //Line renderer reference
    public float m_pullForce;
    private int m_lrIndex;                      //Current lineRenderer array index
    private LineRendererGraphic[] m_gr;         //Line renderer pool
    //private bool m_anchored;
    private Hook[] m_hooks;
    public float m_maxHangTime;
    private float m_hangTime;
    //private bool m_continueEnabled;           //flag for whether script should be disabled at end of update
    public float m_bulletSpread;
    //public float m_recoil;

    public ParticleSystem[] m_onCollisionParticles;
    public ParticleSystem[] m_onFireParticles;

    // Use this for initialization
    void Start()
    {
        Initialize();
        enabled = false;
        //m_anchored = false;

        m_hooks = new Hook[m_amount];

        for (int i = 0; i < m_hooks.Length; i++)
        {
            m_hooks[i] = new Hook();
            //Debug.Log(m_anchors[i]);
        }

        m_gr = new LineRendererGraphic[m_amount];
        for (int i = 0; i < m_gr.Length; i++)
        {
            m_gr[i] = new LineRendererGraphic();
            m_gr[i].m_go = Instantiate(m_lineRendererRef, transform);
            m_gr[i].m_lr = m_gr[i].m_go.GetComponent<LineRenderer>();
            m_gr[i].m_lr.enabled = false;
        }

        //m_weapon.m_inputup += Release;
    }

    // Update is called once per frame
    void Update()
    {
        HandleAnchors();
        HandleInput();
        HandleGraphics();
    }

    public override void OnFire()
    {
        enabled = true;
        m_weapon.enabled = true;
        Hook();//!!!INCOMPLETE: only first muzzle active
    }

    private void HandleInput()
    {
        if (m_weapon.m_currentInputType == InputType.Stay)
        {
            Reel();
        }
        else if (m_weapon.m_currentInputType == InputType.Up)
        {
            for (int i = 0; i < m_amount; i++)
            {
                m_hooks[i].m_anchored = false;
            }
        }
    }

    public void HandleAnchors()
    {
        for (int i = 0; i < m_amount; i++)
        {
            if (m_hooks[i].m_transform == null || !m_hooks[i].m_transform.gameObject.activeSelf)
            {
                m_hooks[i].m_anchored = false;
            }
        }
    }

    protected override void HandleGraphics()
    {
        for (int i = 0; i < m_amount; i++)
        {
            if (m_gr[i].m_lr.enabled)
            {

                m_gr[i].m_lr.SetPosition(0, m_muzzles[0].position);
                if (m_hooks[i].m_anchored)
                {
                    m_gr[i].m_lr.SetPosition(1, m_hooks[i].m_transform.position + m_hooks[i].m_localOffset);
                }

                if (m_gr[i].m_fadeTimer <= 0)
                {
                    m_gr[i].m_lr.enabled = false;
                }
                else
                {
                    Color c;
                    c = m_gr[i].m_lr.startColor;
                    c.a = (m_gr[i].m_fadeTimer / m_rendererActiveTime);
                    m_gr[i].m_lr.startColor = c;

                    c = m_gr[i].m_lr.endColor;
                    c.a = (m_gr[i].m_fadeTimer / m_rendererActiveTime);
                    m_gr[i].m_lr.endColor = c;
                }

                m_gr[i].m_fadeTimer = Mathf.Max(0, m_gr[i].m_fadeTimer - Time.deltaTime);
            }
        }
    }

    //Think fishing
    private void Hook()
    {
        //for (int i = 0; i < m_amount; i++)
        //{m_gr[i].m_fadeTimer = m_rendererActiveTime;}

        Vector3 weaponAcc;//!!! rename for accuracy
        weaponAcc = m_weapon.transform.position + transform.forward * 100;
        weaponAcc += GetDeviance(m_weapon.transform, m_weapon.m_accuracy);

        for (int i = 0; i < m_amount; i++)
        {
            //Line renderer graphics setup
            m_gr[m_lrIndex].m_fadeTimer = m_rendererActiveTime;
            m_gr[m_lrIndex].m_lr.enabled = true;
            m_gr[m_lrIndex].m_lr.SetPosition(0, transform.position);// m_muzzle.position);

            //Handle bullet trajectory
            Vector3 inaccuracy = GetDeviance(m_muzzles[0].transform, m_bulletSpread);

            Vector3 point;
            RaycastHit info;
            point = weaponAcc;
            point += inaccuracy;
            m_muzzles[0].transform.LookAt(point);

            //Handle collisions
            Physics.Raycast(m_muzzles[0].position, m_muzzles[0].forward, out info, 1000, m_mask);
            if (info.collider != null)
            {
                // Functionality
                //m_anchored = true;
                m_hangTime = m_maxHangTime;
                m_hooks[i].m_anchored = true;
                m_hooks[i].m_transform = info.transform;
                m_hooks[i].m_rb = info.transform.root.GetComponent<Rigidbody>();

                //if (m_anchors[i].m_rb != null){ Debug.Log(m_anchors[i].m_rb.gameObject.name); }

                if (m_hooks[i].m_rb == null)
                {m_hooks[i].m_localOffset = info.point - info.transform.position;}
                else
                {m_hooks[i].m_localOffset = Vector3.zero;}

                // Graphics
                m_gr[m_lrIndex].m_lr.SetPosition(1, info.point);
                for (int j = 0; j < m_onCollisionParticles.Length; j++)
                {
                    m_onCollisionParticles[j].transform.position = info.point;
                    m_onCollisionParticles[j].transform.forward = -2 * Vector3.Dot(m_muzzles[0].forward, info.normal) * info.normal + m_muzzles[0].forward;
                    m_onCollisionParticles[j].Play();
                }

                HitOtherAtPoint(info.collider, info.point, m_damage);   
            }
            else
            {
                m_hooks[i].m_anchored = false;
                m_gr[m_lrIndex].m_lr.SetPosition(1, point);
                //Debug.DrawRay(a_muzzle.position, transform.forward * 100, Color.black);
            }

            //Cycle through line renderers
            m_lrIndex = ++m_lrIndex % m_gr.Length;
        }

        //a_weapon.m_bulletSpread = Mathf.Min(a_weapon.m_maxSpread, a_weapon.m_bulletSpread + a_weapon.m_recoil);
        m_weapon.m_accuracy = Mathf.Min(m_weapon.m_maxAimDeviancy, m_weapon.m_accuracy + m_recoil);
    }

    private void Reel()
    {
        if (m_hangTime <= 0) return;

        for (int i = 0; i < m_amount; i++)
        {
            if (m_hooks[i].m_anchored)
            {
                Vector3 point = m_hooks[i].m_transform.position + m_hooks[i].m_localOffset;
                if (m_hooks[i].m_rb == null)
                {
                    m_userRb.AddForce((point - m_userRb.transform.position).normalized * m_pullForce * Time.deltaTime, ForceMode.Impulse);
                }
                else
                {
                    if (m_hooks[i].m_rb.mass == 0 || m_userRb.mass == 0) { return; }

                    float ratio = m_userRb.mass/(m_userRb.mass + m_hooks[i].m_rb.mass);
                    Vector3 dir = (point - m_userRb.transform.position).normalized * m_pullForce;

                    //Debug.Log(ratio);

                    m_userRb.AddForce(dir * (1-ratio) * Time.deltaTime, ForceMode.Impulse);
                    m_hooks[i].m_rb.AddForce(-dir * ratio * Time.deltaTime, ForceMode.Impulse);
                    //Debug.Log((1 - ratio) - (ratio));
                }

                m_gr[i].m_fadeTimer = m_rendererActiveTime;
            }
        }

        m_hangTime = Mathf.Max(0, m_hangTime - Time.deltaTime);
    }

    //private void Release()
    //{
    //    m_anchored = false;
    //}

    public override void Bind(Weapon a_weapon)
    {
        //Debug.Log("Name: " + gameObject.name);
        //Debug.Log("User: " + m_weapon.m_player);
        //Debug.Log("Rigidbody exists? : " + (m_weapon.m_player.m_rb != null));
        m_weapon = a_weapon;
        m_userRb = a_weapon.m_player.m_rb;
        if (m_userRb.mass == 0) { m_userRb.mass = 0.000000000001f; }
    }

    public override void Unbind()
    {
        m_userRb = null;
        enabled = false;
    }
}

public class Hook
{
    public bool m_anchored;
    public Transform m_transform;
    public Vector3 m_localOffset;
    public Rigidbody m_rb;
}
