﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastFireEffect : FireEffect
{
    //public float m_amount;                      //Amount of raycasts fired
    public float m_damageFalloff;               //damage decrease per unit of distance
    public float m_bulletSpread;
    //public float m_recoil;

    [Header("Graphics")]
    private int m_lrIndex;                      //Current lineRenderer array index
    //public bool m_fadeRenderers;
    public float m_trailVelocity;               //Amount the trailRenderer start position moves towards the end position (Assuming 2 vector positions only)
    public int m_particleEmitOnHit;             //Amount of particles to emit on hit
    public float m_rendererActiveTime;
    public int m_lineRendererPoolSize;
    private LineRendererGraphic[] m_gr;         //Line renderer pool
    public GameObject m_lineRendererRef;        //Line renderer reference
    public ParticleSystem[] m_onCollisionParticles;
    //public ParticleSystem[] m_onFireParticles;
    
    public int shotTotalDamage;

    private void Start()
    {
        Initialize();
        enabled = false;

        m_gr = new LineRendererGraphic[m_lineRendererPoolSize];
        for (int i = 0; i < m_gr.Length; i++)
        {
            m_gr[i] = new LineRendererGraphic();
            m_gr[i].m_go = Instantiate(m_lineRendererRef, transform);
            m_gr[i].m_lr = m_gr[i].m_go.GetComponent<LineRenderer>();
            m_gr[i].m_lr.enabled = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        enabled = false;

        HandleGraphics();
    }

    public override void OnFire()
    {
        enabled = true;
        m_weapon.enabled = true;
        //PlayOnFireParticles();

        Vector3 weaponAcc;//!!! rename for accuracy
        weaponAcc = m_weapon.transform.position + transform.forward * 100;
        weaponAcc += GetDeviance(m_weapon.transform, m_weapon.m_accuracy);

        for (int i = 0; i < m_amount; i++)
        {
            //Line renderer graphics setup
            m_gr[m_lrIndex].m_fadeTimer = m_rendererActiveTime;
            m_gr[m_lrIndex].m_lr.enabled = true;

            for (int j = 0; j < m_gr[m_lrIndex].m_lr.positionCount; j++)
            {
                m_gr[m_lrIndex].m_lr.SetPosition(j, m_muzzles[0].position);
            }

            //Handle bullet trajectory
            Vector3 inaccuracy = GetDeviance(m_muzzles[0].transform, m_bulletSpread);

            Vector3 point;
            RaycastHit info;
            point = weaponAcc;
            point += inaccuracy;
            m_muzzles[0].transform.LookAt(point);

            //Handle collisions
            Physics.Raycast(m_muzzles[0].position, m_muzzles[0].forward, out info, 1000, m_mask);
            if (info.collider != null)
            {
                m_gr[m_lrIndex].m_lr.SetPosition(m_gr[m_lrIndex].m_lr.positionCount-1, info.point);

                for (int j = 0; j < m_onCollisionParticles.Length; j++)
                {
                    m_onCollisionParticles[j].transform.position = info.point;
                    m_onCollisionParticles[j].transform.forward = -2 * Vector3.Dot(m_muzzles[0].forward, info.normal) * info.normal + m_muzzles[0].forward;
                    m_onCollisionParticles[j].Emit(m_particleEmitOnHit);
                    //m_onCollisionParticles[j].Play();
                }

                //Debug.DrawLine(a_muzzle.position, info.point, Color.black, .4f);

                //Distances of less than 1 have no damage falloff
                int damage = (int)Mathf.Clamp((m_damage) - m_damageFalloff * (info.distance - 1), 0, m_damage);
                HitOtherAtPoint(info.collider, info.point,damage);
                

                shotTotalDamage += damage;
            }
            else
            {
                m_gr[m_lrIndex].m_lr.SetPosition(m_gr[m_lrIndex].m_lr.positionCount - 1, point);
                //Debug.DrawRay(a_muzzle.position, transform.forward * 100, Color.black);
            }

            //Cycle through line renderers
            m_lrIndex = ++m_lrIndex % m_gr.Length;
        }

        //a_weapon.m_bulletSpread = Mathf.Min(a_weapon.m_maxSpread, a_weapon.m_bulletSpread + a_weapon.m_recoil);
        m_weapon.m_accuracy = Mathf.Min(m_weapon.m_maxAimDeviancy, m_weapon.m_accuracy + m_recoil);
        


        //Debug.Log("Damage:" + shotTotalDamage);
        shotTotalDamage = 0;
    }

    //private void PlayOnFireParticles()
    //{
    //    for (int i = 0; i < m_onFireParticles.Length; i++)
    //    {
    //        m_onFireParticles[i].Play();
    //    }
    //}

    //A hell of my own creation
    protected override void HandleGraphics()
    {
        //Color c;
        Vector3 dist;
        for (int i = 0; i < m_gr.Length; i++)
        {
            m_gr[i].m_fadeTimer = Mathf.Max(0, m_gr[i].m_fadeTimer - Time.deltaTime);

            if (m_gr[i].m_fadeTimer > 0)
            {
                //Move lineRenderer tail towards head
                dist = m_gr[i].m_lr.GetPosition(m_gr[m_lrIndex].m_lr.positionCount - 1) - m_gr[i].m_lr.GetPosition(0);

                if (dist.magnitude < 0.001f)
                {
                    m_gr[i].m_lr.enabled = false;
                }
                else
                {
                    m_gr[i].m_lr.SetPosition(0, m_gr[i].m_lr.GetPosition(0) + dist.normalized * Mathf.Min(m_trailVelocity * Time.deltaTime, dist.magnitude));

                    Vector3 pos0 = m_gr[i].m_lr.GetPosition(0);
                    //Vector3 pos1 = m_gr[i].m_lr.GetPosition(1);
                    for (int j = 1; j < m_gr[i].m_lr.positionCount - 1; j++)
                    {
                        m_gr[i].m_lr.SetPosition(j, pos0 + (dist/ m_gr[i].m_lr.positionCount) * j);
                    }

                    //if (m_fadeRenderers)
                    //{
                    //    Color c;
                    //    c = m_gr[i].m_lr.startColor;
                    //    c.a = (m_gr[i].m_fadeTimer / m_rendererActiveTime);
                    //    m_gr[i].m_lr.startColor = c;

                    //    c = m_gr[i].m_lr.endColor;
                    //    c.a = (m_gr[i].m_fadeTimer / m_rendererActiveTime);
                    //    m_gr[i].m_lr.endColor = c;
                    //}
                }
            }
            else
            {
                m_gr[i].m_lr.enabled = false;
            }

            enabled = enabled | m_gr[i].m_lr.enabled;
        }
    }

    public override void Bind(Weapon a_weapon)
    {
    }

    public override void Unbind()
    {
    }
}

