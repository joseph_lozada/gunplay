﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Weapon))]
abstract public class FireEffect : MonoBehaviour
{
    public enum FireMode
    {
        //SingleShot,   //Just use semiAuto with 1 max bullet lol same thing
        SemiAuto,
        FullAuto,
        Charge,
        Burst,
        ENUM_END
    }
    public enum MuzzleMode
    {
        All,
        Cycle,
        ENUM_END
    }

    protected Weapon m_weapon;
    public int m_amount;
    public float m_recoil;
    public int m_damage;
    public float m_force;
    public LayerMask m_mask;       //Which layers can interact with this
    public FireMode m_firemode;
    public MuzzleMode m_muzzlemode;
    public Transform[] m_muzzles;

    // Use this for initialization
    void Start ()
    {
        Initialize();
    }

    protected void Initialize()
    {
        m_weapon = GetComponent<Weapon>();
        SetFireMode(m_firemode);
    }

    public virtual void SetFireMode(FireMode a_firemode)
    {
        //Debug.Log("FireMode Set");
        switch (a_firemode)
        {
            case FireMode.SemiAuto:
                m_weapon.m_triggerInput[0, (int)InputType.Down] += OnFire;
                if (m_weapon.m_ammo != null)
                {
                    m_weapon.m_triggerInput[0, (int)InputType.Down] += m_weapon.m_ammo.Consume;
                }
                break;
            case FireMode.FullAuto:
                m_weapon.m_triggerInput[0, (int)InputType.Down] += OnFire;
                m_weapon.m_triggerInput[0, (int)InputType.Stay] += OnFire;

                if (m_weapon.m_ammo != null)
                {
                    m_weapon.m_triggerInput[0, (int)InputType.Down] += m_weapon.m_ammo.Consume;
                    m_weapon.m_triggerInput[0, (int)InputType.Stay] += m_weapon.m_ammo.Consume;
                }
                break;
            default:
                break;
        }
    }

    public abstract void OnFire();

    protected void HitOtherAtPoint(Collider other, Vector3 point, int a_damage)
    {
        Health hpTemp = other.GetComponent<Health>();
        if (hpTemp != null)
        {
            hpTemp.TakeDamage(a_damage);
        }

        Rigidbody rbTemp = other.transform.root.GetComponent<Rigidbody>();
        if (rbTemp != null)
        {
            rbTemp.AddForceAtPosition(transform.forward * m_force, point, ForceMode.Impulse);
        }
    }
    
    protected Vector3 GetDeviance(Transform a_direction, float a_trueDeviance)
    {
        //NOTE: GIMBAL LOCKED AND ABANDONED
        ////Right angle rotations for up and right
        //v1.Set(a_direction.x, -a_direction.z, a_direction.y);
        //v2.Set(-a_direction.z, a_direction.y, a_direction.x);

        Vector3 v = new Vector3();
        //Set mirection
        v = (transform.up * UnityEngine.Random.Range(-1.0f, 1.0f)) + (transform.right * UnityEngine.Random.Range(-1.0f, 1.0f));
        //Set magnitude
        a_trueDeviance *= 0.5f;
        v = v.normalized * UnityEngine.Random.Range(-a_trueDeviance, a_trueDeviance);

        return v;
    }

    protected abstract void HandleGraphics();

    public abstract void Bind(Weapon a_weapon);     //  Get needed info about user
    public abstract void Unbind();                  //  Remove info about user
}

public class LineRendererGraphic
{
    public GameObject m_go;
    public LineRenderer m_lr = null;
    public float m_fadeTimer;
}