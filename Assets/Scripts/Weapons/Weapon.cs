﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum InputType
{
    Down,
    Stay,
    Up,
    ENUM_END
}

public class Weapon : MonoBehaviour
{    
    public delegate void FireInput();
    public FireInput[,] m_triggerInput = new FireInput[2,(int)InputType.ENUM_END];

    //[HideInInspector]
    public Player m_player;
    //[Header("Ammo")]
    //public Sprite m_ammoSprite;
    //public int m_ammo;
    //public int m_ammoConsumption;
    //public int m_maxAmmo;
    //public int m_reloadAmount;
    public float m_reloadTime;


    //!!!PROTO: move these to another component
    [Header("Fire Rate")]
    public float m_baseCooldown;
    public float m_cooldownScale = 1;
    //[HideInInspector]public float m_currentCooldownScale;
    [SerializeField] private float m_multiplier;
    public float m_multiplierGravity;
    public float m_maxMultiplier;
    [SerializeField]private float m_cooldownTimer;

    [Header("Accuracy")]
    //public float m_spreadGravity;                //How fast the gun returns to minimum bullet spread
    //public float m_minSpread;
    //public float m_maxSpread;
    //public float m_bulletSpread;                  //Bullet spread at a range of 100 units
    //public float m_recoil;                          //Added bullet spread after firing
    public float m_accuracyStabilityGravity;        //How fast the gun returns to minimum aim deviance
    public float m_minAimDeviancy;               
    public float m_maxAimDeviancy;
    [HideInInspector] public float m_accuracy;

    //[Header("Misc")]
    public GameObject m_model;
    public FireEffect[] m_onFireEffects;
    public Ammunition m_ammo;
    //public Transform[] m_muzzle;
    [HideInInspector] public InputType m_currentInputType;

    private void Start()
    {
        Initialize();
    }

    protected void Initialize()
    {
        enabled = false;
        m_accuracy = m_minAimDeviancy;
    }

    private void Update()
    {
        enabled = false;
        CooldownHandler();
        StabilizeWeapon();
    }

    public virtual void CooldownHandler()
    {
        m_cooldownTimer = Mathf.Max(0, m_cooldownTimer - Time.deltaTime);

        if (m_currentInputType != InputType.Stay)//!!!HACK: hack?
        { m_multiplier = Mathf.Max(1, m_multiplier - Time.deltaTime * m_multiplierGravity); }

        enabled = enabled | (m_cooldownTimer > 0) | (m_multiplier > 1);
    }
    
    public void WeaponFire(InputType a_input)
    {
        enabled = true;
        m_currentInputType = a_input;

        if (m_cooldownTimer > 0) { return; }
        if (m_ammo != null && m_ammo.CheckEmpty()) {  return; }
        OnWeaponFire(a_input);
        m_multiplier = Math.Min(m_maxMultiplier, m_multiplier+1);
        m_cooldownTimer = m_baseCooldown * Mathf.Pow(m_cooldownScale,m_multiplier);//!!!PROTO: maybe costly

        //if (m_ammo <= 0) { HUDManager.SetReloadGraphic(0); }//!!!INEFFICIENT: inefficient?
    }

    //return whether weapon has fired
    private void OnWeaponFire(InputType a_input)
    {
        switch (a_input)
        {
            case InputType.Down:
                if (m_triggerInput[0, (int)InputType.Down] != null)
                { m_triggerInput[0, (int)InputType.Down].Invoke(); }
                break;
            case InputType.Stay:
                if (m_triggerInput[0, (int)InputType.Stay] != null)
                { m_triggerInput[0, (int)InputType.Stay].Invoke(); }
                break;
            case InputType.Up:
                if (m_triggerInput[0, (int)InputType.Up] != null)
                { m_triggerInput[0, (int)InputType.Up].Invoke(); }
                break;
            default:
                break;
        }
    }

    public void StabilizeWeapon()
    {
        //m_bulletSpread = Mathf.Max(m_minSpread, m_bulletSpread - m_spreadGravity * Time.deltaTime);
        m_accuracy = Mathf.Max(m_minAimDeviancy, m_accuracy - (m_accuracyStabilityGravity * Time.deltaTime));
        enabled = enabled | (m_accuracy > m_minAimDeviancy);
    }

    public void Reloading(float a_percentage)
    {
        //Debug.Log(a_percentage);
        //HUDManager.instance.m_ammoGraphic.m_ammoCount.text = m_currentAmount.ToString();
        HUDManager.SetReloadGraphic(a_percentage);
    }

    public void Reload()
    {
        m_ammo.Reload();
        HUDManager.SetReloadGraphic(1);
    }

    public bool CheckEmpty()
    {
        return m_ammo.CheckEmpty();
    }

    public void Bind(Player a_player , bool a_HUDBind = true)
    {
        m_player = a_player;

        //Remove rigidbody, if any
        Rigidbody r = GetComponent<Rigidbody>();
        { if (r != null) { Destroy(r); } }

        //Remove Despawn script, if any
        Despawn d = GetComponent<Despawn>();
        { if (d != null) { Destroy(d); } }

        //Disable light, if any
        Light l = GetComponent<Light>();
        { if (l != null) { l.enabled = false; } }

        //Disable collider/s
        Collider[] c = GetComponents<Collider>();
        {
            for (int i = 0; i < c.Length; i++)
            { c[i].enabled = false; }
        }

        m_ammo.Bind(this);
        foreach (FireEffect f in m_onFireEffects)
        {
            f.Bind(this);
        }
    }

    public void Unbind()
    {
        gameObject.AddComponent<Despawn>().m_Timer = 30;//!!!HARDCODED

        //Enable light, if any
        Light l = GetComponent<Light>();
        { if (l != null) { l.enabled = true; } }

        //Enable colliders on weapon
        Collider[] c = GetComponents<Collider>();
        for (int i = 0; i < c.Length; i++)
        { c[i].enabled = true; }

        m_ammo.Unbind();
        foreach (FireEffect f in m_onFireEffects)
        {
            f.Unbind();
        }
    }
}

[RequireComponent(typeof(Weapon))]
public abstract class Ammunition : MonoBehaviour
{
    public Sprite m_ammoSprite;
    public float m_scale;
    public float m_gravity;

    public abstract void Bind(Weapon a_weapon);
    public abstract void Unbind();
    public abstract void Consume();     // Return whether or not weapon is allowed to fire
    public abstract void Reload();          // Returns whether or not weapon reload can be initiated
    public abstract bool AllowReload();
    public abstract bool CheckEmpty();
    public abstract void HandleHUD();

}

[RequireComponent(typeof(Weapon))]
public abstract class Aim : MonoBehaviour
{
    public abstract void AimWeapon();
}