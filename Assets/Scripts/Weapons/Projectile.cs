﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public LayerMask m_mask;
    public Rigidbody m_rb;
    [HideInInspector] public int m_damage;
    public float m_lifespan;
    [HideInInspector] public float m_force;
    private float m_lifetime;
    public onHitEffect m_onHitEffect;
    [HideInInspector] public Vector3 m_prevPos;
    //private Vector3 m_origScale;
    public ParticleSystem m_onHitParticle;
    //private TrailRenderer m_tr;
    private bool m_active = true;          // whether the collider can still hit others

    public enum onHitEffect
    {
        Bounce,
        Attach,
        Ricochet,
        Despawn,
        ENUM_END
    }

	// Use this for initialization
	void Start ()
    {
        m_rb = GetComponent<Rigidbody>();
        //m_tr = GetComponent<TrailRenderer>();
        m_prevPos = transform.position;
        //m_origScale = transform.lossyScale;
    }
	
	// Update is called once per frame
	void Update ()
    {
        HandleLifetime();
        RayInterpolate();
	}

    private void RayInterpolate()
    {
        if (!m_active) { return; }

        Vector3 direction = (transform.position - m_prevPos);
        RaycastHit info;
        Physics.Raycast(m_prevPos, direction, out info, direction.magnitude, m_mask);
        m_prevPos = transform.position;

        if (info.collider != null)
        {
            transform.position = info.point;
            onHit(info.collider);
        }

        Debug.DrawLine(m_prevPos, transform.position, Color.red, 1);
    }

    private void HandleLifetime()
    {
        m_lifetime += Time.deltaTime;
        if (m_lifetime > m_lifespan)
        {
            gameObject.SetActive(false);
            //Destroy(gameObject);
        }
    }

    //private void OnEnable()
    //{
    //    Reset();
    //}

    //public void Reset()
    //{
    //    m_lifetime = 0;
    //    m_active = true;
    //    transform.SetParent(null);
    //    //transform.localScale = m_origScale;
    //    if (m_tr != null) { m_tr.enabled = true; m_tr.Clear(); }
    //    if (m_rb == null)
    //    {m_rb = gameObject.AddComponent<Rigidbody>(); }
    //    m_rb.velocity = Vector3.zero;
    //}

    private void OnCollisionEnter(Collision collision)
    {
        onHit(collision.collider);   
    }

    private void OnTriggerEnter(Collider other)
    {
        onHit(other);
    }

    private void onHit(Collider other)
    {
        if (!m_active) { return; }
        m_active = false;
        Health h = other.GetComponent<Health>();
        if (h != null)
        {
            h.TakeDamage(m_damage);
        }

        if (m_onHitParticle != null) { m_onHitParticle.Play(); }

        //Debug.Log("Hit: " + other.name.ToString());
        switch (m_onHitEffect)
        {
            case onHitEffect.Bounce:
                //Debug.Log("Bounce");
                m_rb.useGravity = true;
                break;
            case onHitEffect.Attach:

                Rigidbody r = other.transform.root.GetComponent<Rigidbody>();
                if (r != null)
                {
                    Debug.Log(other.transform.root);
                    r.AddForceAtPosition(transform.forward * m_force, other.ClosestPoint(transform.position));
                }

                Destroy(m_rb);
                transform.parent = other.transform;

                //if (m_tr != null) { m_tr.enabled = false; }
                GetComponent<Collider>().enabled = false;
                break;
            default:
                break;
        }
    }

    private void Remain()
    {
        
    }
}
