﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthAmmo : Ammunition
{
    //!!!PROTOTYPE: int-only

    Health m_userHealth;
    public int m_currentAmount;
    [SerializeField] private int m_maxAmount;
    [SerializeField] private int m_consumption;     //Amount consumed each time weapon is fired
    [SerializeField] private int m_reloadAmount;    

	// Use this for initialization
	void Start ()
    {
        enabled = false;
	}
	
	// Update is called once per frame
	void Update ()
    {
    }

    public override void Consume()
    {
        m_currentAmount = Math.Max(0, m_currentAmount - m_consumption);
        HandleHUD();
    }

    public override bool AllowReload()
    {
        return (m_currentAmount < m_maxAmount);
    }

    public override void Reload()
    {
        if (m_currentAmount >= m_maxAmount) { return; }

        int cost = Math.Min(m_maxAmount - m_currentAmount, m_reloadAmount);
        //m_userHealth.TakeDamage(x);
        m_currentAmount += cost;
        HUDManager.instance.m_ammoGraphic.m_ammoCount.text = m_currentAmount.ToString();

        //Pseudo TakeDamage to ignore invulnerability
        HUDManager.SetVignetteAlpha(1);
        HUDManager.instance.m_HPBar.fillAmount = (float)m_userHealth.m_current / m_userHealth.m_max;
        m_userHealth.m_current = Math.Max(0, m_userHealth.m_current - cost);
        if (m_userHealth.m_current <= 0) { m_userHealth.TakeDamage(1); }
    }

    public override bool CheckEmpty()
    {
        return (m_currentAmount <= 0);
    }

    public override void Bind(Weapon a_weapon)
    {
        m_userHealth = a_weapon.m_player.m_HP;
    }

    public override void Unbind()
    {
        m_userHealth = null;
    }

    public override void HandleHUD()
    {
        HUDManager.instance.m_ammoGraphic.m_ammoCount.text = m_currentAmount.ToString();
        if (m_currentAmount <= 0)
        {
            HUDManager.SetReloadGraphic(0);
        }
        else
        {
            HUDManager.SetReloadGraphic(1);
        }
    }

}
