﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntAmmo : Ammunition
{
    public int m_currentAmount;
    [SerializeField] private int m_maxAmount;
    [SerializeField] private int m_consumption;     //Amount consumed each time weapon is fired
    [SerializeField] private int m_reloadAmount;    
    
	// Use this for initialization
	void Start ()
    {
        enabled = false;
        //HUDManager.instance.m_ammoGraphic.m_ammoCount.text = m_currentAmount.ToString();
    }
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public override void Consume()
    {
        if (m_consumption == 0) { return; }
        m_currentAmount = Math.Max(0, m_currentAmount - m_consumption);
        HandleHUD();
    }

    public override void HandleHUD()
    {
        HUDManager.instance.m_ammoGraphic.m_ammoCount.text = m_currentAmount.ToString();
        if (m_currentAmount <= 0)
        {
            HUDManager.SetReloadGraphic(0);
        }
        else
        {
            HUDManager.SetReloadGraphic(1);
        }
    }

    public override bool AllowReload()
    {
        return (m_currentAmount < m_maxAmount);
    }
    public override void Reload()
    {
        m_currentAmount = Math.Min(m_maxAmount, m_currentAmount + m_reloadAmount);
        HUDManager.instance.m_ammoGraphic.m_ammoCount.text = m_currentAmount.ToString();
    }
    public override bool CheckEmpty()
    {
        return (m_currentAmount <= 0);
    }

    public override void Bind(Weapon a_weapon)
    {
    }

    public override void Unbind()
    {
    }
}
