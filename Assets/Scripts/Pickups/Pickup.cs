﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Pickup : MonoBehaviour
{
    public float m_despawnTimer;
    public abstract void OnPickup();
}
