﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropManager : Manager
{
    public static DropManager instance;

    public float m_despawnTime;
    public float[] m_chanceRatio;           //ratios are: Failure, Common, Uncommon, Rare
    public GameObject[] m_commonDrops;
    public GameObject[] m_uncommonDrops;
    public GameObject[] m_rareDrops;
    //public ParticleSystem m_spawnParticles;
    
    // Use this for initialization
    void Start ()
    {
        Initialize();
    }

    public override void Initialize()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance == this)
        {
            return;
        }
        else
        {
            Destroy(this);
            return;
        }

        //check for errors
        if (m_chanceRatio.Length != 4) { Debug.LogError("Array MUST be size 4"); return; }

        //normalize chanceRatio
        float total = 0;
        for (int i = 0; i < m_chanceRatio.Length; i++)
        {
            if (m_chanceRatio[i] < 0) { m_chanceRatio[i] = 0; }
            total += m_chanceRatio[i];
        }
        for (int i = 0; i < m_chanceRatio.Length; i++)
        {
            m_chanceRatio[i] /= total;
        }
    }

    //As in, roll the dice
    private static GameObject Roll(float a_modifier)
    {
        //Roll item tier
        float x = UnityEngine.Random.Range(0.0f, 1.0f) * a_modifier;
        float total = 0;
        for (int i = 0; i < instance.m_chanceRatio.Length; i++)
        {
            total += instance.m_chanceRatio[i];

            if (x < total)
            { return RollItem(i); }
        }

        return null;
    }

    private static GameObject RollItem(int a_tier)
    {
        switch (a_tier)
        {
            case 1:
                return instance.m_commonDrops[UnityEngine.Random.Range(0, instance.m_commonDrops.Length)];
            case 2:
                return instance.m_uncommonDrops[UnityEngine.Random.Range(0, instance.m_uncommonDrops.Length)];
            case 3:
                return instance.m_rareDrops[UnityEngine.Random.Range(0, instance.m_rareDrops.Length)];
            case 0:
            default:
                return null;
        }
    }

    public static void Drop(Vector3 a_position, float a_modifier)
    {
        GameObject loot = Roll(a_modifier);
        if (loot == null) { return; }

        loot = Instantiate(loot);
        loot.transform.position = a_position;
        loot.AddComponent<Despawn>().m_Timer = instance.m_despawnTime;
    }
}
