﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class ScoreManager : Manager
{
    public static ScoreManager instance;
    private UserStats m_currentStats;
    private ScoreList m_scoreList;
    private int m_rank = -1;
    private const int m_maxScoreboardSize = 256;

    // Use this for initialization
    void Start()
    {
        //enabled = false;
        //Initialize();
    }

    public override void Initialize()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance == this)
        {
            return;
        }
        else
        {
            Destroy(this);
            return;
        }

        Debug.Log("Initializing ScoreManager");
        m_scoreList = new ScoreList();
        m_currentStats = new UserStats();

        Load();

        //!!!HACK
        HUDManager.instance.m_highScoreInputField.onEndEdit.AddListener(delegate
        {
            //!!!HARDCODED
            m_currentStats.m_name = HUDManager.instance.m_highScoreInputField.text.Substring(0,Math.Min(15, HUDManager.instance.m_highScoreInputField.text.Length));
            if (HUDManager.instance.m_highScoreInputField.text.Length <= 0) { return; }

            HUDManager.instance.m_highScoreInputField.gameObject.SetActive(false);
            AddScore(ref m_scoreList, ref m_currentStats);
            DisplayScore();
            Save();
            Debug.Log("NAME: " + m_currentStats.m_name);
        });

        //Debug.Log(Application.persistentDataPath);
    }

    public static void AddBossKill()
    {
        if (instance == null) { Debug.LogWarning("No ScoreManager instance"); return; }
        ++instance.m_currentStats.m_bossKills;
        instance.m_currentStats.m_score += 10; //!!!HARDCODED
    }

    public static void AddMobKill()
    {
        if (instance == null) { Debug.LogWarning("No ScoreManager instance"); return; }
        ++instance.m_currentStats.m_mobKills;
        instance.m_currentStats.m_score += 1; //!!!HARDCODED
    }

    public static void Save()
    {
        if (instance == null) { Debug.LogWarning("ScoreManager error: No ScoreManager instance"); return; }
        if (instance.m_currentStats == null) { Debug.Log("ScoreManager error: No score"); return; }

        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/Scores.sav");
        ScoreList data;

        data = new ScoreList();
        data = instance.m_scoreList;

        bf.Serialize(file, data);
        file.Close();

        Debug.Log("Score saved");
    }

    public static void Load()
    {
        if (instance == null) { return; }

        //If save file not found, create new scoreList
        if (!File.Exists(Application.persistentDataPath + "/Scores.sav"))
        {
            Debug.LogWarning("No Save file");
            return;
        }

        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Open(Application.persistentDataPath + "/Scores.sav", FileMode.Open);
        instance.m_scoreList = (ScoreList)bf.Deserialize(file);
        file.Close();

        Debug.Log("Scores loaded");
    }

    private void AddScore(ref ScoreList a_list, ref UserStats a_newData)
    {
        if (a_list.m_stats.Count >= m_maxScoreboardSize)
        {
            // Find lowest
            int minScore = int.MaxValue;
            int minScoreIndex = 0;
            for (int i = 0; i < a_list.m_stats.Count; i++)
            {
                if (a_list.m_stats[i].m_score < minScore)
                {
                    minScore = a_list.m_stats[i].m_score;
                    minScoreIndex = i;
                }
            }

            // If new data is lowest, return
            if (minScore > a_newData.m_score) { return; }

            // Remove lowest
            a_list.m_stats.RemoveAt(minScoreIndex); //!!!NOTE: always last pos
        }

        // Insert new data in appropriate place (0 == highest, 19 == lowest)
        int newIndex = 0;
        for (int i = 0; i < a_list.m_stats.Count; i++)
        {
            if (a_newData.m_score >= a_list.m_stats[i].m_score)
            {
                newIndex = i;
                break;
            }
        }

        UserStats data = new UserStats(a_newData);

        a_list.m_stats.Insert(newIndex, data);
        m_rank = ++newIndex;
    }

    public static void DisplayScore()
    {
        if (instance == null) { return; }

        HUDManager.instance.m_highScorePanel.SetActive(true);
        SetScores();
    }


    //!!!HARDCODED
    private static void SetScores()
    {
        if (instance.m_scoreList == null) { Debug.LogWarning("ScoreManager error: list null"); return; }
        if (instance.m_scoreList.m_stats.Count <= 0) { Debug.LogWarning("ScoreManager error: list length 0"); return; }

        //HUDManager.instance.m_highScores.text = "HIGH SCORES\n";
        if (instance.m_rank >= 0)
        {
            HUDManager.instance.m_highScores.text = "YOUR RANK:" + instance.m_rank + "\n\n";
        }
        else
        {
            HUDManager.instance.m_highScores.text = "";
        }

        int iterations = Math.Min(50, instance.m_scoreList.m_stats.Count); //!!!HARDCODED
        for (int i = 0; i < iterations; i++)//!!!HARDCODED
        {
            HUDManager.instance.m_highScores.text += "[" + (i+1) + "] ";
            HUDManager.instance.m_highScores.text += instance.m_scoreList.m_stats[i].m_name.ToString() + '\n';
            HUDManager.instance.m_highScores.text += instance.m_scoreList.m_stats[i].m_score.ToString() + " points\n\n";
            //HUDManager.instance.m_highScores.text += "Mob kills: " + instance.m_scoreList.m_stats[i].m_mobKills.ToString() + '\n';
            //HUDManager.instance.m_highScores.text += "Boss kills: " + instance.m_scoreList.m_stats[i].m_bossKills.ToString() + '\n';
        }
    }

    //private void OnGUI()
    //{
    //    if (!m_displayLeaderboard) { return; }
    //    GUI.Box(new Rect(30, 30, 150, 30), "Boss Kills: " + m_stats.m_bossKills.ToString());
    //    GUI.Box(new Rect(30, 60, 150, 30), "Mob Kills: " + m_stats.m_mobKills.ToString());
    //}
}

[Serializable]
public class ScoreList
{
    public ScoreList()
    {
        m_stats = new List<UserStats>();
    }

    public List<UserStats> m_stats;
}

[Serializable]
public class UserStats
{
    public UserStats()
    {
        m_name      = "<?>";
        m_score     = 0;
        m_mobKills  = 0;
        m_bossKills = 0;
    }

    public UserStats(UserStats a_other)
    {
        m_name      = a_other.m_name;
        m_score     = a_other.m_score;
        m_mobKills  = a_other.m_mobKills;
        m_bossKills = a_other.m_bossKills; 
    }

    public string   m_name;
    public int      m_score;
    public int      m_mobKills;
    public float    m_bossKills;
}
