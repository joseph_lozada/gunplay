﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * NOTES:
 * no system in place to prevent picking a spot with a pillar already in it yet
 */

[RequireComponent(typeof(GameManager))]
public class WorldManager : Manager
{
    public static WorldManager instance;

    public Vector3 m_gameworldDimensions;
    public int m_wPartitions;
    public int m_hPartitions;
    public int m_maxPillars;
    public float m_pillarPersistTime;
    public float m_pillarSpawnTime;
    private float m_pillarSpawnTimer;
    public float m_pillarRiseSpeed;
    public float m_pillarFallSpeed;
    public float m_pillarHeight;
    public GameObject m_basePillar;
    public GameObject m_baseWorldCube;

    private bool[,] m_worldGrid;
    private Pillar[] m_pillars;
    private int m_pillarIndex;
    private float m_minHeight;

    //private bool m_continueEnabled;

    // Use this for initialization
    public override void Initialize()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance == this)
        {
            return;
        }
        else
        {
            Destroy(this);
            return;
        }

        GenerateBoundaries();

        m_baseWorldCube.SetActive(false); //Just in case

        if (m_wPartitions > 0 && m_hPartitions > 0)
        {
            m_worldGrid = new bool[m_wPartitions, m_hPartitions];
            m_pillars = new Pillar[Mathf.Min(m_maxPillars, m_wPartitions * m_hPartitions)];

            //m_basePillar = GameObject.CreatePrimitive(PrimitiveType.Cube);
            m_basePillar.SetActive(false);
            m_basePillar.transform.localScale = new Vector3(m_gameworldDimensions.x / m_wPartitions, m_pillarHeight, m_gameworldDimensions.z / m_hPartitions);
            for (int i = 0; i < m_maxPillars; i++)
            {
                m_pillars[i] = new Pillar();
                m_pillars[i].m_go = Instantiate(m_basePillar);
                m_pillars[i].m_go.SetActive(false);
            }

            m_minHeight = m_basePillar.transform.localScale.y * -0.5f;
        }
    }

    // Update is called once per frame
    void Update ()
    {
        HandlePillarSpawning();
        HandleActivePillars();
    }

    //private void OnDestroy()
    //{
    //    instance = null;
    //}

    void HandleActivePillars()
    {
        for (int i = 0; i < m_maxPillars; i++)
        {
            if (m_pillars[i].m_go.activeSelf)
            {
                if (m_pillars[i].m_timer > 0)
                {
                    //Debug.Log("Pillar " + i + " timer: " + m_pillars[i].m_timer);
                    m_pillars[i].m_timer = Mathf.Max(0, m_pillars[i].m_timer - Time.deltaTime);
                    m_pillars[i].m_go.transform.position = new Vector3(m_pillars[i].m_go.transform.position.x, Mathf.Min(-m_minHeight, m_pillars[i].m_go.transform.position.y + m_pillarRiseSpeed * Time.deltaTime), m_pillars[i].m_go.transform.position.z);
                }
                else if (m_pillars[i].m_go.transform.position.y > m_minHeight)
                {
                    m_pillars[i].m_go.transform.position = new Vector3(m_pillars[i].m_go.transform.position.x, Mathf.Max(m_minHeight, m_pillars[i].m_go.transform.position.y - m_pillarFallSpeed * Time.deltaTime), m_pillars[i].m_go.transform.position.z);
                }
                else
                {
                    m_pillars[i].m_go.SetActive(false);
                    m_worldGrid[m_pillars[i].xPos, m_pillars[i].yPos] = false;
                }
            }
        }
    }

    void HandlePillarSpawning()
    {
        m_pillarSpawnTimer = Mathf.Max(0, m_pillarSpawnTimer - Time.deltaTime);
        if (m_pillarSpawnTimer <= 0)
        {
            m_pillarSpawnTimer = m_pillarSpawnTime;
            SpawnPillar();
        }
    }

    void SpawnPillar()
    {
        int x, z;
        x = UnityEngine.Random.Range(0, m_wPartitions);
        z = UnityEngine.Random.Range(0, m_hPartitions);

        if (!m_worldGrid[x, z])
        {
            if (!m_pillars[m_pillarIndex].m_go.activeSelf)
            {
                m_pillars[m_pillarIndex].m_timer = m_pillarPersistTime;
                m_pillars[m_pillarIndex].m_go.SetActive(true);
                m_pillars[m_pillarIndex].m_go.transform.position = new Vector3
                    (
                    m_basePillar.transform.localScale.x * x - m_gameworldDimensions.x * 0.5f + m_basePillar.transform.localScale.x * 0.5f * Mathf.Sign(x),
                    -m_basePillar.transform.localScale.y * 0.5f,
                    m_basePillar.transform.localScale.z * z - m_gameworldDimensions.z * 0.5f + m_basePillar.transform.localScale.z * 0.5f * Mathf.Sign(z)
                    );
                m_pillars[m_pillarIndex].xPos = x;
                m_pillars[m_pillarIndex].yPos = z;

                //Debug.Log("Pillar spawned at: " + m_pillars[m_pillarIndex].m_go.transform.position + "" + m_pillars[m_pillarIndex].m_go.activeSelf);
                m_pillarIndex = ++m_pillarIndex % m_maxPillars;
                m_worldGrid[x, z] = true;
            }
        }
    }

    //!!! MONOLITHIC CODE
    void GenerateBoundaries()
    {
        const float width = 1000;
        float height = m_gameworldDimensions.y;

        GameObject world = new GameObject("World");
        GameObject goTemp = null;

        m_baseWorldCube.SetActive(true);

        //Generate pit
        goTemp = Instantiate(m_baseWorldCube);
        goTemp.transform.parent = world.transform;
        goTemp.transform.localScale = new Vector3(width - m_gameworldDimensions.x * 0.5f, height,width * 2);
        goTemp.transform.position = new Vector3(m_gameworldDimensions.x * 0.5f + width * 0.5f - m_gameworldDimensions.x * 0.25f, height * 0.5f, 0); // right boundary
        
        goTemp = Instantiate(m_baseWorldCube);
        goTemp.transform.parent = world.transform;
        goTemp.transform.localScale = new Vector3(width - m_gameworldDimensions.x * 0.5f, height, width * 2);
        goTemp.transform.position = new Vector3(-(m_gameworldDimensions.x * 0.5f + width * 0.5f) + m_gameworldDimensions.x * 0.25f, height * 0.5f, 0); // left boundary
        
        goTemp = Instantiate(m_baseWorldCube);
        goTemp.transform.parent = world.transform;
        goTemp.transform.localScale = new Vector3(m_gameworldDimensions.x, height, width - m_gameworldDimensions.z * 0.5f);
        goTemp.transform.position = new Vector3(0, height * 0.5f, m_gameworldDimensions.z * 0.5f + width * 0.5f - m_gameworldDimensions.z * 0.25f); // upper boundary
        
        goTemp = Instantiate(m_baseWorldCube);
        goTemp.transform.parent = world.transform;
        goTemp.transform.localScale = new Vector3(m_gameworldDimensions.x, height, width - m_gameworldDimensions.z * 0.5f);
        goTemp.transform.position = new Vector3(0, height * 0.5f, -(m_gameworldDimensions.z * 0.5f + width * 0.5f) + m_gameworldDimensions.z * 0.25f); // lower boundary
        
        goTemp = Instantiate(m_baseWorldCube);
        goTemp.transform.parent = world.transform;
        goTemp.transform.localScale = new Vector3(m_gameworldDimensions.x * 2, width, m_gameworldDimensions.z * 2);
        goTemp.transform.position = new Vector3(0, -width * 0.5f, 0); // floor

        m_baseWorldCube.SetActive(false);

        //Generate invisible boundaries        
        LayerMask l = LayerMask.NameToLayer("CheckOnlyPlayer");

        goTemp = GameObject.CreatePrimitive(PrimitiveType.Cube);
        goTemp.transform.parent = world.transform;
        goTemp.transform.localScale = new Vector3(width - m_gameworldDimensions.x * 0.5f, m_gameworldDimensions.y + width, width * 2);
        goTemp.transform.position = new Vector3(m_gameworldDimensions.x * 0.5f + width * 0.5f - m_gameworldDimensions.x * 0.25f, width * 0.5f, 0); // right boundary
        goTemp.layer = l;
        Destroy(goTemp.GetComponent<Renderer>());
        Destroy(goTemp.GetComponent<MeshFilter>());

        goTemp = GameObject.CreatePrimitive(PrimitiveType.Cube);
        goTemp.transform.parent = world.transform;
        goTemp.transform.localScale = new Vector3(width - m_gameworldDimensions.x * 0.5f, m_gameworldDimensions.y + width, width * 2);
        goTemp.transform.position = new Vector3(-(m_gameworldDimensions.x * 0.5f + width * 0.5f) + m_gameworldDimensions.x * 0.25f, width * 0.5f, 0); // left boundary
        goTemp.layer = l;
        Destroy(goTemp.GetComponent<Renderer>());
        Destroy(goTemp.GetComponent<MeshFilter>());

        goTemp = GameObject.CreatePrimitive(PrimitiveType.Cube);
        goTemp.transform.parent = world.transform;
        goTemp.transform.localScale = new Vector3(m_gameworldDimensions.x, m_gameworldDimensions.y + width, width - m_gameworldDimensions.z * 0.5f);
        goTemp.transform.position = new Vector3(0, width * 0.5f, m_gameworldDimensions.z * 0.5f + width * 0.5f - m_gameworldDimensions.z * 0.25f); // upper boundary
        goTemp.layer = l;
        Destroy(goTemp.GetComponent<Renderer>());
        Destroy(goTemp.GetComponent<MeshFilter>());

        goTemp = GameObject.CreatePrimitive(PrimitiveType.Cube);
        goTemp.transform.parent = world.transform;
        goTemp.transform.localScale = new Vector3(m_gameworldDimensions.x, m_gameworldDimensions.y + width, width - m_gameworldDimensions.z * 0.5f);
        goTemp.transform.position = new Vector3(0, width * 0.5f, -(m_gameworldDimensions.z * 0.5f + width * 0.5f) + m_gameworldDimensions.z * 0.25f); // lower boundary
        goTemp.layer = l;
        Destroy(goTemp.GetComponent<Renderer>());
        Destroy(goTemp.GetComponent<MeshFilter>());
    }
}

public class Pillar
{
    public GameObject m_go;
    public float m_timer;
    public int xPos;
    public int yPos;
}