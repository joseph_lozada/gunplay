﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(GameManager))]
public class HUDManager : Manager
{
    public static HUDManager instance;
    public Camera m_minimapCamera;
    public RectTransform[] m_crosshairs;
    public GameObject m_gameOverScreen;
    public GameObject m_gameplayUI;
    private Vector2[] m_baseCrosshairSize;
    public Image m_HPBar;
    public Image m_energyBar;
    public Image m_damageVignette;
    public float m_damageVignetteFadeSpeed;
    public Image m_reloadGraphic;
    public GameObject m_weaponSlotParent;
    public Image m_weaponSlot;
    public GameObject m_highScorePanel;
    public Text m_highScores;
    public InputField m_highScoreInputField;
    private Image[] m_weaponSlots;

    public AmmoGraphic m_ammoGraphic;

    private static Color c; //reusable temp storage

    public override void Initialize()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance == this)
        {
            return;
        }
        else
        {
            Destroy(this);
            return;
        }

        Debug.Log("Initializing HUDManager");
        //Set crosshairSize
        m_baseCrosshairSize = new Vector2[2];
        m_baseCrosshairSize[0] = m_crosshairs[0].sizeDelta;
        m_baseCrosshairSize[1] = m_crosshairs[1].sizeDelta;

        //Set damageVignette
        m_damageVignette.gameObject.SetActive(true);
        c = m_damageVignette.color;
        c.a = 0;
        m_damageVignette.color = c;

        //for()

        m_ammoGraphic.Initialize();
    }

    private void Update()
    {
        HandleDamageVignette();
    }

    private void HandleDamageVignette()
    {
        c = m_damageVignette.color;
        c.a = Mathf.Max(0, c.a - m_damageVignetteFadeSpeed * Time.deltaTime);
        m_damageVignette.color = c;
    }

    public static void SetVignetteAlpha(float a_alpha)
    {
        c = instance.m_damageVignette.color;
        c.a = Mathf.Clamp(a_alpha, 0, 1);
        instance.m_damageVignette.color = c;
    }

    //private void OnDestroy()
    //{
    //    instance = null;
    //}

    public static void SetReloadGraphic(float a_fillAmount)
    {
        instance.m_reloadGraphic.fillAmount = a_fillAmount;
        //instance.m_reloadGraphic.gameObject.SetActive(instance.m_reloadGraphic.fillAmount < 1);
    }

    //public static void OnFire()
    //{
    //    instance.m_ammoGraphic.OnFire();
    //}

    public static void SetWeapon(Weapon a_weapon)
    {
        instance.m_ammoGraphic.SetWeapon(a_weapon);

        ////!!!HACK: hack?
        if (a_weapon != null && a_weapon.m_ammo != null) { a_weapon.m_ammo.HandleHUD(); return; }
        SetReloadGraphic(0);
    }

    public static void SetCrossHairScale(int a_index, float a_scale)
    {
        //const float x = 0.5f / 1.4f;
        const float x = .75f;

        instance.m_crosshairs[a_index].sizeDelta = instance.m_baseCrosshairSize[a_index] * Mathf.Max(2, a_scale * x);
    }

    public static IEnumerator ScrambleCamera(Camera a_cam)
    {
        a_cam.clearFlags = CameraClearFlags.SolidColor;
        a_cam.cullingMask = 0;

        float timer = 0.2f;//!!!HARDCODED
        return new WaitUntil(delegate ()
        {
            timer = Mathf.Max(0, timer - Time.unscaledDeltaTime);
            if (timer <= 0)
            {
                a_cam.backgroundColor = Color.black;
            }
            else
            {
                a_cam.backgroundColor = new Color(Random.Range(0, 2), Random.Range(0, 2), Random.Range(0, 2), 1);
            }

            return (timer <= 0);
        });
    }
}
