﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//!!!Fragile
public class AmmoGraphic : MonoBehaviour
{
    public Weapon m_weapon;
    public GameObject m_ammoImageGroup;
    public Image m_ammoImage;
    public Image m_ammoSilhouette;
    //public Image m_mask;
    public Text m_ammoCount;
    public float m_cycleSpeed;              //how many rounds get cycled through per second

    //private int m_imageIndex;
    //private Image[] m_imageArray;
    public Sprite m_defaultSprite;

    public void Initialize()
    {
        enabled = false;

        //for (int i = 0; i < 10; i++)
        //{
        //    Instantiate(m_ammoImage, m_ammoImage.transform.parent);
        //}
        //Destroy(m_ammoImage.gameObject);

        m_ammoCount.text = "0";
        //m_imageIndex = 0;
    }
	
	// Update is called once per frame
	public void Update ()
    {
        enabled = false;
	}

    public void SetWeapon(Weapon a_weapon)
    {
        if (a_weapon != null)
        {
            m_weapon = a_weapon;
            //m_ammoCount.text = m_weapon.m_ammo.ToString();
            
            if (m_weapon.m_ammo.m_ammoSprite != null)
            { m_ammoImage.sprite = m_weapon.m_ammo.m_ammoSprite; }
            else
            { m_ammoImage.sprite = m_defaultSprite; }
        }
        else
        {
            m_ammoCount.text = "0";
            m_ammoImage.sprite = m_defaultSprite;
        }

        m_ammoSilhouette.sprite = m_ammoImage.sprite;
    }

    //public void OnFire()
    //{
    //    m_ammoCount.text = m_weapon.m_ammo.ToString();
    //}

    void HandleImages()
    {
        Debug.Log("INCOMPLETE");
        Debug.Break();
    }

    void HandleMask()
    {
        Debug.Log("INCOMPLETE");
        Debug.Break();
    }
}
