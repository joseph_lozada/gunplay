﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : Manager
{
    public static GameManager instance;
    public static Player m_player;
    public static bool isPlaying = true;

    // Use this for initialization
    void Start ()
    {
        Initialize();
	}

    // Update is called once per frame
    void Update()
    {
        //DebuggingTools();
    }

    private void DebuggingTools()
    {
        HandleCursorMode();

        if (Input.GetKeyDown(KeyCode.P))
        {WaveManager.instance.SpawnWave();}

        if (Input.GetKeyDown(KeyCode.O))
        { ScoreManager.Save(); }

        if (Input.GetKeyDown(KeyCode.I))
        { ScoreManager.Load(); }

        if (Input.GetKeyDown(KeyCode.Y))
        { ScoreManager.DisplayScore(); }
    }

    public override void Initialize()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance == this)
        {
            return;
        }
        else
        {
            Destroy(this);
            return;
        }

        Debug.Log("Initializing GameManager");
        GetComponent<DropManager>().Initialize();
        GetComponent<WorldManager>().Initialize();
        GetComponent<WaveManager>().Initialize();
        GetComponent<HUDManager>().Initialize();
        //GetComponent<ScoreManager>().Initialize();

        Manager[] m = GetComponentsInChildren<Manager>();

        for (int i = 0; i < m.Length; i++)
        {
            m[i].Initialize();
        }
    }

    public static void SetPlayer(Player a_player)
    {
        if (m_player == null)
        {
            m_player = a_player;
        }
        else if (a_player == m_player)
        {
            return;
        }
        else
        {
            Destroy(a_player);
        }
    }

    //private void OnDestroy()
    //{
    //    m_player = null;
    //    isPlaying = true;
    //}

    //public static void StartGame()
    //{
    //}

    public static void GameOver()
    {
        //Unlock mouse constraints
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;

        ScoreManager.Load();
        ScoreManager.DisplayScore();
        HUDManager.instance.m_gameOverScreen.SetActive(true);
        Time.timeScale = 0;
        //HUDManager.instance.m_highScoreInputField.gameObject.SetActive(true);
        //ScoreManager.Save();
        //ScoreManager.DisplayScore();
    }

    public void ResetGame()
    {
        //Destroy(gameObject);
        //SceneManager.LoadScene(SceneManager.GetActiveScene().name);

        //reset persistent variables
        Time.timeScale = 1;
        //isPlaying = true;
    }

    void HandleCursorMode()
    {
        if (Input.GetKeyUp(KeyCode.L))
        {
            //Debug.Log("CursorMode Toggled");
            if (Cursor.lockState != CursorLockMode.None)
            {
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
            }
            else
            {
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
            }
        }
    }
}
