﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(GameManager))]
public class WaveManager : Manager
{
    public static WaveManager instance;

    private int m_TotalEnemies;
    public int m_waveNo = 0;
    public float m_waveTime;
    private float m_waveTimer;
    public float buffer;
    public int m_bossWaveInterval;//!!! RENAME FOR ACCURACY
    //public GameObject m_mobMinimapMarker;
    //public GameObject m_bossMinimapMarker;

    public BaseEnemy[] m_waveMobs;
    public BaseEnemy[] m_bosses;
    
    public override void Initialize()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance == this)
        {
            return;
        }
        else
        {
            Destroy(this);
            return;
        }

        if (enabled)
        {
            SpawnWave();
        }
    }

    private void Update()
    {
        
    }

    //private void OnDestroy()
    //{
    //    instance = null;
    //}

    public void SpawnWave()
    {
        ++m_waveNo;
        BaseEnemy e;

        if (m_bossWaveInterval <= 0) { m_bossWaveInterval = 1; }
        if (m_waveNo % m_bossWaveInterval != 0) 
        {
            Debug.Log("Spawning Mobs");
            for (int i = 0; i < m_waveNo; i++)
            {
                e = SpawnEnemy(m_waveMobs[(Random.Range(0, m_waveMobs.Length))]);
                PlaceAroundBorder(e.gameObject.transform);
                AttachLoot(e, 1);
                e.GetComponent<Health>().OnDeath += ScoreManager.AddMobKill;
            }
        }
        else
        {
            Debug.Log("Spawning Boss");
            e = SpawnEnemy(m_bosses[(Random.Range(0, m_bosses.Length))]);
            e.transform.position = Vector3.up * (WorldManager.instance.m_gameworldDimensions.y + 500);
            AttachLoot(e, 10);
            e.GetComponent<Health>().OnDeath += ScoreManager.AddBossKill;
        }

    }

    BaseEnemy SpawnEnemy(BaseEnemy a_enemy)
    {
        BaseEnemy r = Instantiate(a_enemy);

        r.transform.LookAt(new Vector3(0, r.transform.position.y, 0));
        r.GetComponent<Health>().OnDeath += EnemyDeath;

        m_TotalEnemies++;
        return r;
    }

    void EnemyDeath()
    {
        m_TotalEnemies--;

        if (m_TotalEnemies <= 0)
        {
            Debug.Log("Wave " + m_waveNo + ". Spawning wave");
            SpawnWave();
        }
    }

    void PlaceAroundBorder(Transform a_transform)
    {
        Vector3 dim = WorldManager.instance.m_gameworldDimensions;
        if (Random.Range(0, 2) == 0)
        {
            //spawn left/right
            a_transform.position = new Vector3((dim.x * 0.5f + buffer) * (Random.Range(0, 2) * 2 - 1), dim.y + 5, Random.Range(-dim.z - buffer, dim.z + buffer) * 0.5f);//!!!HARDCODED
        }
        else
        {
            //spawn forward,back
            a_transform.position = new Vector3(Random.Range(-dim.x - buffer, dim.x + buffer) * 0.5f + buffer, dim.y + 5, (dim.z * 0.5f + buffer) * (Random.Range(0, 2) * 2 - 1));//!!!HARDCODED
        }
    }

    void AttachLoot(BaseEnemy a_enemy, float a_modifier)
    {
        a_enemy.GetComponent<Health>().OnDeath += delegate { DropManager.Drop(a_enemy.transform.position, a_modifier); };
    }
}
