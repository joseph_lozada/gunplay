﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Health))]
public class DestroyOnDeath : MonoBehaviour
{
    public GameObject[] m_objects;

    // Use this for initialization
    void Start()
    {
        enabled = false;
        GetComponent<Health>().OnDeath += DestroyObjects;
    }

    private void DestroyObjects()
    {
        for (int i = 0; i < m_objects.Length; i++)
        {
            Destroy(m_objects[i]);
        }
    }
}
