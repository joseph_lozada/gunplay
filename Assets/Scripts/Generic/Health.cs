﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    public enum DamageSource
    {
        Unknown,
        Self,
        Enemy,
        Collision,
        ENUM_END
    }

    public delegate void HealthListener();

    public HealthListener OnTakeDamage;
    public HealthListener OnDeath;

    public int m_max;
    public int m_current;

    public float m_invincibilityTime;
    private float m_iTimer;
    public DamageSource m_lastDamageSource;

    public void TakeDamage(int a_damage)
    {
        if (m_iTimer > 0 || a_damage <= 0) { return; }

        enabled = true;
        m_iTimer = m_invincibilityTime;
        m_current = Mathf.Clamp(m_current - a_damage, 0, m_current);

        if (OnTakeDamage != null)
        { OnTakeDamage.Invoke(); }
    }

    public void TakeDamage(int a_damage, DamageSource a_source)
    {
        m_lastDamageSource = a_source;
        TakeDamage(a_damage);
    }

    private void Update()
    {
        enabled = false;
        if (m_current <= 0) { Die(); return; }
        HandleInvincibility();
    }

    void Die()
    {
        if (OnDeath != null)
        {
            OnDeath.Invoke();
        }
        else
        {
            DefaultDeath();
        }

        OnDeath = null;
    }

    public void DefaultDeath()
    {
        StartCoroutine(DefaultDeathCoroutine());
    }

    private IEnumerator DefaultDeathCoroutine()
    {
        if (gameObject.GetComponent<Rigidbody>() == null)
        {
            gameObject.AddComponent<Rigidbody>();
        }

        gameObject.layer = LayerMask.NameToLayer("IgnoreGameplay");
        transform.parent = null;
        yield return new WaitForSeconds(5);//!!!HARDCODED
        Destroy(gameObject);
    }

    private void HandleInvincibility()
    {
        m_iTimer = Mathf.Max(0, m_iTimer -= Time.deltaTime);
        enabled = (m_iTimer > 0);
    }
}
