﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class SetViewportSize : MonoBehaviour
{
    public enum Anchor
    {
        TOP_LEFT,
        TOP_RIGHT,
        BOTTOM_LEFT,
        BOTTOM_RIGHT
    }

    public Anchor m_anchor;
    public Vector2 m_pixelSize;
    public Vector2 m_buffer;
    private Camera m_camera;

	// Use this for initialization
	void Start ()
    {
        m_camera = GetComponent<Camera>();
        SetScale();
        Destroy(this);
	}
	
	// Update is called once per frame
	void Update ()
    {
	}

    void SetScale()
    {
        Rect r = new Rect(0,0,1,1);
        Vector2 nBuffer = m_buffer; //normalized buffer

        m_camera.rect = r;  //make sure that values are from a full-sized camera

        r.width = (m_pixelSize.x / m_camera.pixelWidth);
        r.height = (m_pixelSize.y / m_camera.pixelHeight);

        nBuffer.x = m_buffer.x / m_camera.pixelWidth;
        nBuffer.y = m_buffer.y / m_camera.pixelHeight;

        //Debug.Log("pixelsize: " + m_pixelSize + " ratio: (" + m_camera.pixelHeight + ", " + m_camera.pixelWidth + "}");
        //Debug.Log(r.width + ", " + r.height);

        switch (m_anchor)
        {
            case Anchor.TOP_LEFT:
                r.position = new Vector2(0 + nBuffer.x, (1 - r.height) - nBuffer.y);
                break;
            case Anchor.TOP_RIGHT:
                r.position = new Vector2((1 - r.width) - nBuffer.x, (1 - r.height)- nBuffer.y);
                break;
            case Anchor.BOTTOM_LEFT:
                r.position = new Vector2(0, 0) + nBuffer;
                break;
            case Anchor.BOTTOM_RIGHT:
                r.position = new Vector2((1 - r.width) - nBuffer.x, nBuffer.y);
                break;
            default:
                break;
        }

        m_camera.rect = r;
    }
}
