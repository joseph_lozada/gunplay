﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldBounds : MonoBehaviour
{
    float m_force = 10;

	// Use this for initialization
	void Start ()
    {
        enabled = false;
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (transform.position.x > WorldManager.instance.m_gameworldDimensions.x * 0.5f ||
            transform.position.x <-WorldManager.instance.m_gameworldDimensions.x * 0.5f ||
            transform.position.z > WorldManager.instance.m_gameworldDimensions.z * 0.5f ||
            transform.position.z <-WorldManager.instance.m_gameworldDimensions.z * 0.5f
            )
        {
            Vector3 dir = -transform.position;
            GetComponent<Rigidbody>().AddForce(dir * m_force, ForceMode.Impulse);
        }
	}
}
