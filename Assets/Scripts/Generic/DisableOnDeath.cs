﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Health))]
public class DisableOnDeath : MonoBehaviour
{

    public GameObject[] m_objects;

	// Use this for initialization
	void Start ()
    {
        enabled = false;
        GetComponent<Health>().OnDeath += DisableObjects;
	}

    private void DisableObjects()
    {
        for (int i = 0; i < m_objects.Length; i++)
        {
            m_objects[i].SetActive(false);
        }
    }
}
