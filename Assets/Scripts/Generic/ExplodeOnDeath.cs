﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Health))]
public class ExplodeOnDeath : MonoBehaviour
{
    public float m_timer;
    public float m_radius;
    public int m_damage;
    public float m_force;                       // Can go below 0 for implosions
    public float m_minFalloffDistance;
    public float m_damageFalloff;
    public float m_forceFalloff;
    public LayerMask m_mask;
    public ParticleSystem m_particles;

	// Use this for initialization
	void Start ()
    {
        enabled = false;
        SetBehaviour();
	}

    //!!!UNUSUAL: only 1 behaviour so far
    void SetBehaviour()
    {
        GetComponent<Health>().OnDeath += OnDeath;
    }

    void OnDeath()
    {
        StartCoroutine(StartExplosion());
    }

    IEnumerator StartExplosion()
    {
        yield return new WaitForSeconds(m_timer);

        Rigidbody rb;
        Health h;
        Vector3 dir;

        Collider[] c = Physics.OverlapSphere(transform.position, m_radius, m_mask);

        if (m_force > 0)
        {
            for (int i = 0; i < c.Length; i++)
            {
                dir = c[i].transform.position - transform.position;
                rb = c[i].GetComponent<Rigidbody>();
                if (rb != null)
                {
                    rb.AddForce(dir.normalized * m_force * (dir.magnitude + m_minFalloffDistance) * (1 - m_forceFalloff), ForceMode.Impulse);
                }
            }
        }

        if (m_damage > 0)
        {
            for (int i = 0; i < c.Length; i++)
            {
                dir = c[i].transform.position - transform.position;
                h = c[i].GetComponent<Health>();
                if (h != null)
                {
                    h.TakeDamage((int)Mathf.Clamp(m_damage * (dir.magnitude + m_minFalloffDistance) * (1 - m_damageFalloff), 0, m_damage));
                }
            }
        }

        if (m_particles != null)
        {
            m_particles.Play();
        }
    }
}
