﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Health))]
public class SpawnOnDeath : Spawner
{
    // Use this for initialization
    void Start()
    {
        SetBehaviour();
        enabled = false;
    }

    protected override void DropAll()
    {
        GetComponent<Health>().OnDeath += delegate ()
            {
                for (int i = 0; i < m_iterations; i++)
                {
                    for (int j = 0; j < m_SpawnList.Length; j++)
                    {
                        Instantiate(m_SpawnList[j]).transform.position = transform.position + new Vector3(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f));
                    }
                }
            };
    }

    protected override void DropCycle()
    {
        int i = 0;

        GetComponent<Health>().OnDeath += delegate ()
        {
            for (int j = 0; i < m_iterations; j++)
            {
                Instantiate(m_SpawnList[i]).transform.position = transform.position + new Vector3(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f));
                i = ++i % m_SpawnList.Length;
            }
        };
    }

    protected override void DropRandom()
    {
        GetComponent<Health>().OnDeath += delegate ()
        {
            for (int i = 0; i < m_iterations; i++)
            {
                Instantiate(m_SpawnList[Random.Range(0, m_SpawnList.Length)]).transform.position = transform.position + new Vector3(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f));
            }
        };
    }
}
