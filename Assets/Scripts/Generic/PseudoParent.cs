﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PseudoParent : MonoBehaviour
{
    public Transform m_parent;

	// Use this for initialization
	void Start ()
    {
        if (transform.root != transform) { Destroy(this); }
	}
	
	// Update is called once per frame
	void Update ()
    {
        transform.position = m_parent.transform.position;
	}
}
