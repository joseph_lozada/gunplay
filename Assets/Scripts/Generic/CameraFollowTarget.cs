﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowTarget : MonoBehaviour
{
    public float m_moveSpeed;
    public Transform m_target;
    public Vector3 m_offset;

    private Vector3 dir;

    // Use this for initialization
    void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (m_target != null)
        {
            dir = m_target.position - transform.position;
            transform.position += dir.normalized * Mathf.Min(dir.magnitude, m_moveSpeed * Time.deltaTime);
            //transform.rotation = m_target.rotation;
        }
        else
        {
            enabled = false;
        }
	}
}
