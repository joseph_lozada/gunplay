﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Spawner : MonoBehaviour
{
    public enum Mode
    {
        All,
        Cycle,
        Random,
        ENUM_END,
    }

    public Mode m_mode;
    public GameObject[] m_SpawnList;
    public int m_iterations;

    protected void SetBehaviour()
    {
        switch (m_mode)
        {
            case Mode.All:
                DropAll();
                break;
            case Mode.Cycle:
                DropCycle();
                break;
            case Mode.Random:
                DropRandom();
                break;
            default:
                break;
        }
    }

    protected abstract void DropAll();
    protected abstract void DropCycle();
    protected abstract void DropRandom();
}
