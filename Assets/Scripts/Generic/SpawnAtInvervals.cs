﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnAtInvervals : Spawner
{
    public float m_interval;
    private float m_timer;
    public bool m_loop;
    private delegate void Spawn();
    private Spawn m_spawn;

    // Use this for initialization
    void Start ()
    {
        if (m_interval < 1) { Debug.LogError("LOW INTERVAL. REJECTED."); Destroy(this); return; }
        m_timer = m_interval;
        SetBehaviour();
	}
	
	// Update is called once per frame
	void Update ()
    {
        m_timer -= Time.deltaTime;
        if (m_timer <= 0)
        {
            m_spawn.Invoke();
            m_timer += m_interval;
        }
	}

    protected override void DropAll()
    {
        m_spawn += delegate ()
        {
            for (int i = 0; i < m_iterations; i++)
            {
                for (int j = 0; j < m_SpawnList.Length; j++)
                {
                    Instantiate(m_SpawnList[j]).transform.position = transform.position + new Vector3(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f));
                }
            }
        };
    }

    protected override void DropCycle()
    {
        int i = 0;

        m_spawn += delegate ()
        {
            for (int j = 0; i < m_iterations; j++)
            {
                Instantiate(m_SpawnList[i]).transform.position = transform.position + new Vector3(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f));
                i = ++i % m_SpawnList.Length;
            }
        };
    }

    protected override void DropRandom()
    {
        m_spawn += delegate ()
        {
            for (int i = 0; i < m_iterations; i++)
            {
                Instantiate(m_SpawnList[Random.Range(0, m_SpawnList.Length)]).transform.position = transform.position + new Vector3(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f));
            }
        };
    }
}
