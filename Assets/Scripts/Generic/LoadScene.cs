﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadScene : MonoBehaviour
{
    public float m_delay;

    public void LoadSceneByName(string a_name)
    {
        StartCoroutine(LoadCoroutine(a_name));
    }

    public void LoadSceneByIndex(int a_index)
    {
        StartCoroutine(LoadCoroutine(a_index));
    }

    IEnumerator LoadCoroutine(string a_name)
    {
        yield return new WaitForSeconds(m_delay);
        SceneManager.LoadScene(a_name);
    }

    IEnumerator LoadCoroutine(int a_index)
    {
        yield return new WaitForSeconds(m_delay);
        SceneManager.LoadScene(a_index);
    }
}
