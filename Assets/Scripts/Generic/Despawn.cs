﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Despawn : MonoBehaviour
{
    public Despawn(float a_timer)
    {
        m_Timer = a_timer;
    }

    public float m_Timer;
	
	// Update is called once per frame
	void Update ()
    {
        m_Timer -= Time.deltaTime;
        if (m_Timer <= 0) { Destroy(gameObject); }
	}
}
