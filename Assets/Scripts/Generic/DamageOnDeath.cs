﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof(Health))]
public class DamageOnDeath : MonoBehaviour
{
    public Health m_other;
    public int m_damage;

	// Use this for initialization
	void Start ()
    {
        enabled = false;
        Health h = GetComponent<Health>();
        h.OnDeath += h.DefaultDeath;
        h.OnDeath += DamageOther;
	}
	
	// Update is called once per frame
	void Update ()
    {
        enabled = false;
	}

    private void DamageOther()
    {
        m_other.TakeDamage(m_damage);
    }
}
