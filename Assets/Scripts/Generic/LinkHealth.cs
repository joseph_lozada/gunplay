﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//!!!NOTE: can only damage, not heal
[RequireComponent(typeof(Health))]
public class LinkHealth : MonoBehaviour
{
    private Health m_myHealth;
    public Health m_otherHealth;
    public float m_scale;
    public int prevHP;//!!!HACK

	// Use this for initialization
	void Start ()
    {
        enabled = false;
        m_myHealth = GetComponent<Health>();
        m_myHealth.m_current = m_otherHealth.m_current;
        m_myHealth.m_max = m_otherHealth.m_max;
        m_myHealth.OnTakeDamage += DamageOther;
	}

    private void DamageOther()
    {
        m_otherHealth.TakeDamage((int)((prevHP - m_myHealth.m_current) * m_scale));
        prevHP = m_myHealth.m_current;
    }
}
