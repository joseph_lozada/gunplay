﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Health))]
public class ParentEntity : MonoBehaviour
{
    Health m_hp;
    public float m_despawnTimer;
    Transform[] go;

    // Use this for initialization
    void Start ()
    {
        m_hp = GetComponent<Health>();

        m_hp.OnDeath += OnDeath;
	}

    private void OnDeath()
    {
        Collider c;
        go = GetComponentsInChildren<Transform>();
        //ParentEntity p;

        for (int i = 0; i < go.Length; i++)
        {
            //p = go[i].GetComponent<ParentEntity>();
            //if (p != null && p.gameObject != this)
            //{ Destroy(p); }

            c = go[i].GetComponent<Collider>();
            if (c != null)
            { c.enabled = true; }

            if (go[i] != null) //gameobject)
            { go[i].parent = null; }

            if (go[i].gameObject.GetComponent<Rigidbody>() == null)
            {
                go[i].gameObject.AddComponent<Rigidbody>();
            }

            go[i].gameObject.AddComponent<Despawn>().m_Timer = 3; //!!!HARDCODED
            go[i].gameObject.layer = LayerMask.NameToLayer("IgnoreGameplay");

            //StartCoroutine(DespawnChildren());
        }
    }

    private void OnDestroy()
    {
        m_hp.OnDeath -= delegate
        {
            OnDeath();
        };
    }

    //IEnumerator DespawnChildren()
    //{
    //    yield return new WaitForSeconds(m_despawnTimer);

    //    for (int i = 0; i < go.Length; i++)
    //    {
    //        Destroy(go[i].gameObject);
    //    }
    //}
}
