﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HurtZone : MonoBehaviour
{
    public int m_damage;

    private void OnCollisionEnter(Collision collision)
    {
        Health h = collision.collider.GetComponent<Health>();
        if (h != null) { h.TakeDamage(m_damage); }
    }
}
