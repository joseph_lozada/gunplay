﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetMaterialColor : MonoBehaviour
{
    public Color m_color;
    public Renderer m_renderer;

	// Use this for initialization
	void Start ()
    {
        m_renderer.material.color = m_color;
        Destroy(this);
	}
}
