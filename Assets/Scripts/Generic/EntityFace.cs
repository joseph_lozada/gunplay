﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Health))]
public class EntityFace : MonoBehaviour
{
    public SpriteRenderer m_spriteRenderer;
    public Sprite m_defaultSprite;
    public Sprite m_onTakeDamageSprite;
    public Sprite m_onDeathSprite;

    public float m_hurtDuration = .5f;
    private float m_timer;

    // Use this for initialization
    void Start()
    {
        Health m_health;
        m_health = GetComponent<Health>();
        m_health.OnTakeDamage += OnTakeDamage;
        m_health.OnDeath += OnDeath;
    }
	
	// Update is called once per frame
	void Update ()
    {
        m_timer = Mathf.Max(0, m_timer - Time.deltaTime);
        if (m_timer <= 0)
        {
            m_spriteRenderer.sprite = m_defaultSprite;
            enabled = false;	
        }
	}

    void OnTakeDamage()
    {
        enabled = true;
        m_spriteRenderer.sprite = m_onTakeDamageSprite;
        m_timer = m_hurtDuration;
    }

    void OnDeath()
    {
        m_spriteRenderer.sprite = m_onDeathSprite;
        Destroy(this);
    }
}
