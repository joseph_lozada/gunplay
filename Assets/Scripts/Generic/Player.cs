﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Health))]
public class Player : MonoBehaviour
{
    public Health m_HP;

    [Header("Boost")]
    public float m_energy;
    public float m_maxEnergy;
    public float m_energyFill;
    public float m_energyDrain;
    public float m_boostForce;

    [Header("Movement")]
    public float m_accelSpeed;
    public float m_decelSpeed;
    public float m_maxSpeed;                    // Hard upper limit for speed. speed cannot go higher than this
    public float m_criticalSpeed;               // Soft upper limit, speed decreases rapidly past this
    public float m_criticalDecel;               // damping value at critical speeds
    public float m_jumpSpeed;
    public float m_gravity;

    [Header("Camera")]
    public Camera m_camera;
    public Vector2 m_sensitivity;

    [Header("Weapons")]
    public Weapon m_mainWeapon;
    public Weapon m_subWeapon;
    public Weapon[] m_weaponSlots;              // Weapons inventory
    public LayerMask m_raycastMask;             // Layers that can be targeted
    public float m_pickupRadius;
    public Transform[] m_weaponAnchors;
    //public Weapon m_defaultWeapon;            // Weapon index 0 is default weapon now

    public Rigidbody m_rb;
    private int m_weaponIndex = 0;
    private bool m_isGrounded;
    private bool m_isReloading;
    private float m_reloadTimer;
    private int m_weaponCount;                     //Number of weapons

    // Use this for initialization
    void Start()
    {
        Initialize();
    }

    public void Initialize()
    {
        //Set variables
        FindObjectOfType<GameManager>().Initialize();
        GameManager.SetPlayer(this);

        m_HP = GetComponent<Health>();
        m_rb = GetComponent<Rigidbody>();
        m_mainWeapon = m_weaponSlots[0];

        //Set cursor state
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        //Set death and damage events
        HUDManager.instance.m_HPBar.fillAmount = (float)m_HP.m_current / m_HP.m_max;
        m_HP.OnDeath += OnDeath;
        m_HP.OnTakeDamage += OnTakeDamage;

        if (m_mainWeapon != null) { m_mainWeapon.Bind(this); }
        if (m_subWeapon != null) { m_subWeapon.Bind(this,false); }

        SetWeapon(0);
        countWeapons();
    }

    // Update is called once per frame
    void Update()
    {
        HandleInput();
        HandleHUD();
        HandlePhysics();
        //Debugging
    }

    void HandleInput()
    {
        HandleCamera();
        HandleWeapons();
        HandleMovement();
        HandleBoost();
        HandleJump();
        HandleReload();
        HandleWeaponPickup();
    }

    void HandleMovement()
    {
        //!!!HARDCODED: keycodes
        //Key inputs
        Vector3 dir;
        dir = Vector3.zero;

        if (Input.GetKey(KeyCode.W))
        { dir += transform.forward; }
        if (Input.GetKey(KeyCode.S))
        { dir -= transform.forward; }
        if (Input.GetKey(KeyCode.A))
        { dir -= transform.right; }
        if (Input.GetKey(KeyCode.D))
        { dir += transform.right; }

        //Debug.Log(dir + ", " + dir.magnitude);

        if (dir.magnitude > 0)  //!!! Needless check?
        {
            if (dir.magnitude > 1) { dir.Normalize(); }
            m_rb.AddForce(dir.normalized * m_accelSpeed * Time.deltaTime, ForceMode.VelocityChange);
        }
    }

    void HandleBoost()
    {
        if (Input.GetKey(KeyCode.LeftShift))
        {
            if (m_energy > 0)
            {
                m_energy = Mathf.Max(0, m_energy - (Time.deltaTime * m_energyDrain));
                m_rb.AddForce(transform.forward * m_boostForce * Time.deltaTime, ForceMode.VelocityChange);
            }
        }
        else
        {
            m_energy = Mathf.Min(m_maxEnergy, m_energy + (Time.deltaTime * m_energyFill));
        }
    }

    void HandleJump()
    {
        //Jump
        if (m_isGrounded)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                //Debug.Log("Jump Hit");
                m_rb.AddForce(Vector3.up * m_jumpSpeed, ForceMode.VelocityChange);
            }
        }
    }

    void HandleCamera()
    {
        transform.Rotate(0, Input.GetAxis("Mouse X") * m_sensitivity.x, 0);

        float XRot = m_camera.transform.localEulerAngles.x;
        XRot -= (Input.GetAxis("Mouse Y") * m_sensitivity.y);
        //XRot += m_camera.transform.eulerAngles.x;
        //XRot = Mathf.Clamp(XRot, -90, 90);

        m_camera.transform.eulerAngles = new Vector3(XRot, transform.eulerAngles.y, 0);
    }

    void HandleWeapons()
    {
        HandleWeaponInputs();
        AimWeaponsWithCamera();
    }

    void SetWeapon(int a_index)
    {
        if (a_index >= m_weaponSlots.Length) { return; }
        m_isReloading = false;
        m_weaponIndex = a_index;
        //Debug.Log("WeaponSlot active: " + (m_weapons[m_weaponIndex] != null));

        //Set all weapon slots inactive
        for (int i = 0; i < m_weaponSlots.Length; i++)
        {
            if (m_weaponSlots[i] != null)
            { m_weaponSlots[i].m_model.SetActive(false); }
        }

        //Set current weapon slot active
        if (m_weaponSlots[a_index] != null)
        {
            m_weaponSlots[a_index].m_model.SetActive(true);
        }

        m_mainWeapon = m_weaponSlots[a_index];
        HUDManager.SetWeapon(m_mainWeapon);
    }

    void CycleWeapons()
    {
        m_weaponIndex = ++m_weaponIndex % m_weaponSlots.Length;
        SetWeapon(m_weaponIndex);
    }

    void HandleHUD()
    {
        //HUDManager.SetCrossHairScale(0, (m_mainWeapon.m_accuracy + m_mainWeapon.m_bulletSpread) * 0.5f);
        //HUDManager.SetCrossHairScale(1, (m_subWeapon.m_accuracy + m_subWeapon.m_bulletSpread) * 0.5f);

        if (m_mainWeapon != null)
        {
            HUDManager.SetCrossHairScale(0, (m_mainWeapon.m_accuracy) * 0.5f);
        }
        HUDManager.SetCrossHairScale(1, (m_subWeapon.m_accuracy) * 0.5f);

        HUDManager.instance.m_energyBar.fillAmount = m_energy / m_maxEnergy;

        //Debug.Log(HUDManager.instance.m_HPBar.fillAmount + ", " + HUDManager.instance.m_energyBar.fillAmount);
    }

    //!!!HARDCODED: keycodes
    void HandleWeaponInputs()
    {
        //Main Weapon
        if (!m_isReloading && m_mainWeapon != null)
        {
            if (Input.GetMouseButtonDown(0))
            { m_mainWeapon.WeaponFire(InputType.Down); }
            else if (Input.GetMouseButton(0))
            { m_mainWeapon.WeaponFire(InputType.Stay); }
            else if (Input.GetMouseButtonUp(0))
            {
                m_mainWeapon.WeaponFire(InputType.Up);
                if (m_mainWeapon.CheckEmpty())
                { Reload(); }
            }
        }

        //Sub Weapon
        if (Input.GetMouseButtonDown(1))
        { m_subWeapon.WeaponFire(InputType.Down); }
        else if (Input.GetMouseButton(1))
        { m_subWeapon.WeaponFire(InputType.Stay); }
        else if (Input.GetMouseButtonUp(1))
        {
            m_subWeapon.WeaponFire(InputType.Up);
            //HandleWeaponAmmo(m_subWeapon);
        }

        //Check weapon switch through each slot
        for (KeyCode k = KeyCode.Alpha1; k <= KeyCode.Alpha1 + m_weaponSlots.Length; k++)
        {
            if (Input.GetKeyDown(k))
            { SetWeapon(k - KeyCode.Alpha1); }
        }

        if (Input.GetKeyUp(KeyCode.Q))
        { CycleWeapons(); }

        if (Input.GetKeyUp(KeyCode.G))
        { DropWeapon(); }

        if (Input.GetKey(KeyCode.R))
        { Reload(); }
    }

    private void DropWeapon()
    {
        //if (m_isReloading) { return; }
        m_isReloading = false;
        if (m_weaponIndex == 0) { return; } //Not allowed to drop default weapon
        if (m_weaponSlots[m_weaponIndex] == null) { return; } //Not allowed to drop null weapon

        --m_weaponCount;

        m_weaponSlots[m_weaponIndex].Unbind();
        m_weaponSlots[m_weaponIndex].transform.position += m_weaponSlots[m_weaponIndex].transform.forward * 1.5f;//!!!HARDCODED !!!//HACK

        Rigidbody r = m_weaponSlots[m_weaponIndex].gameObject.AddComponent<Rigidbody>();
        r.AddForce(transform.forward * 10, ForceMode.VelocityChange);

        m_weaponSlots[m_weaponIndex].transform.parent = null;
        m_weaponSlots[m_weaponIndex] = null;
        m_mainWeapon = null;
        HUDManager.SetWeapon(m_mainWeapon);
    }

    //void HandleWeaponAmmo(Weapon a_weapon)
    //{
    //    if (a_weapon.m_ammo <= 0) { Reload(); }
    //}

    void AimWeaponsWithCamera()
    {
        Vector3 point;
        RaycastHit info;
        Physics.Raycast(m_camera.transform.position, m_camera.transform.forward, out info, 1000, m_raycastMask);
        if (info.collider != null)
        {
            if (info.distance > 1)
            { point = info.point; }
            else
            { point = m_camera.transform.position + m_camera.transform.forward * 1; }
        }
        else
        {
            point = m_camera.transform.position + m_camera.transform.forward * 1000;
        }

        if (m_mainWeapon != null)
        { m_mainWeapon.transform.LookAt(point); }
        m_subWeapon.transform.LookAt(point);
    }

    //clamp x/z axis velocity
    void ClampSpeedXZ()
    {
        Vector3 xzVelocity;
        xzVelocity = m_rb.velocity;
        xzVelocity.y = 0;
        float mag = xzVelocity.magnitude;
        Vector3 dir = xzVelocity.normalized;

        //Debug.Log(norm);

        //Deceleration
        if (m_isGrounded)
        {
            if (mag > m_criticalSpeed)
            {
                xzVelocity = xzVelocity - dir * Mathf.Min(mag, m_criticalDecel * Time.deltaTime);
            }
            else
            {
                xzVelocity = xzVelocity - dir * Mathf.Min(mag, m_decelSpeed * Time.deltaTime);
            }

        }

        xzVelocity.y = m_rb.velocity.y;
        m_rb.velocity = xzVelocity;

        //Max speed clamp
        if (mag > m_maxSpeed)
        {
            xzVelocity = dir * m_maxSpeed;
            xzVelocity.y = m_rb.velocity.y;
            m_rb.velocity = xzVelocity;
        }
    }

    void ClampSpeed()
    {
        float mag = m_rb.velocity.magnitude;
        Vector3 dir = m_rb.velocity.normalized;

        //Debug.Log(norm);

        //Deceleration
        if (mag > m_criticalSpeed)
        {
            m_rb.velocity = m_rb.velocity - dir * Mathf.Max(0, m_criticalDecel * Time.deltaTime);
        }
        else
        {
            m_rb.velocity = m_rb.velocity - dir * Mathf.Max(0, m_decelSpeed * Time.deltaTime);
        }

        //Max speed clamp
        m_rb.velocity = dir * Mathf.Min(mag, m_maxSpeed);
    }

    private void HandlePhysics()
    {

        //Debug.Log("BBBB");
        m_isGrounded = Physics.Raycast(transform.position, -Vector3.up, 1.5f);

        m_rb.AddForce(Vector3.down * m_gravity * Time.deltaTime, ForceMode.VelocityChange);

        ClampSpeedXZ();
    }

    private void OnCollisionEnter(Collision collision)
    {
        //Debug.Log(collision.collider.name);
        if (collision.gameObject.layer == LayerMask.GetMask("Enemy")) { return; }

        //Collision damage
        int d = (int)(Mathf.Max(0, collision.impulse.magnitude - m_maxSpeed) * 0.65f); //!!!HARDCODED
        if (d > 0) { m_HP.TakeDamage(d); }// Debug.Log(d); }

        //AutoPickup
        if (m_weaponCount >= m_weaponSlots.Length) { return; }
        //if (collision.gameObject.layer == LayerMask.GetMask("Weapon"))
        {
            Weapon w = collision.collider.GetComponent<Weapon>();
            if (w != null) { AutoPickupWeapon(w); }
        }
    }

    private void countWeapons()
    {
        m_weaponCount = 0;
        for (int i = 0; i < m_weaponSlots.Length; i++)
        {
            if (m_weaponSlots[i] != null)
            { ++m_weaponCount; }
        }
    }

    IEnumerator DeathCam()
    {
        float timer = 5; //!!!HARDCODED
        float dist = 0;
        const float distScale = 3;

        Time.timeScale = 0.95f;
        while (timer > 0)
        {
            timer = Mathf.Max(0, timer - Time.unscaledDeltaTime);
            Time.timeScale = Mathf.Max(0.05f, Time.timeScale - (Time.unscaledDeltaTime * 0.5f)); //!!!HARDCODED
            dist = Mathf.Min(WorldManager.instance.m_gameworldDimensions.y * 5, dist + Time.unscaledDeltaTime * distScale);

            //!!!HARDCODED
            const float orbitsPerSecond = 0.25f * Mathf.PI;

            Destroy(m_camera.GetComponent<CameraFollowTarget>());
            m_camera.transform.parent = null;
            m_camera.transform.position = transform.position + new Vector3(Mathf.Sin(Time.unscaledTime * orbitsPerSecond), 1, Mathf.Cos(Time.unscaledTime * orbitsPerSecond) * 1) * dist;//!!!HARDCODED
            m_camera.transform.LookAt(transform.position);
            yield return new WaitForEndOfFrame();
        }

        StartCoroutine(HUDManager.ScrambleCamera(m_camera));

        yield return new WaitForSecondsRealtime(1);

        GameManager.GameOver();
    }

    void OnDeath()
    {
        enabled = false;

        //Death spasms
        HUDManager.SetVignetteAlpha(0);
        m_rb.constraints = RigidbodyConstraints.None;
        gameObject.layer = LayerMask.NameToLayer("IgnoreGameplay");
        m_rb.AddForce(new Vector3(Random.Range(-20, 20), Random.Range(0, 20), Random.Range(-20, 20)));
        m_rb.AddTorque(new Vector3(Random.Range(-90, 90), Random.Range(-90, 90), Random.Range(-90, 90)));

        //Move weapon anchors to player instead of camera
        for (int i = 0; i < m_weaponAnchors.Length; i++)
        {
            m_weaponAnchors[i].parent = gameObject.transform;
        }

        //Disable Crosshairs
        HUDManager.instance.m_crosshairs[0].gameObject.SetActive(false);
        HUDManager.instance.m_crosshairs[1].gameObject.SetActive(false);

        //Drop Held Weapons
        Collider[] c = m_mainWeapon.GetComponents<Collider>();
        for (int i = 0; i < c.Length; i++)
        { c[i].enabled = true; }
        m_mainWeapon.gameObject.AddComponent<Rigidbody>().AddTorque(Random.Range(-180, 180), Random.Range(-180, 180), Random.Range(-180, 180));
        m_mainWeapon.transform.parent = null;
        m_mainWeapon.Unbind();

        c = m_subWeapon.GetComponents<Collider>();
        for (int i = 0; i < c.Length; i++)
        { c[i].enabled = true; }
        m_subWeapon.gameObject.AddComponent<Rigidbody>().AddTorque(Random.Range(-180, 180), Random.Range(-180, 180), Random.Range(-180, 180));
        m_subWeapon.transform.parent = null;
        m_subWeapon.Unbind();

        //Detach minimap camera
        HUDManager.instance.m_minimapCamera.transform.parent = null;
        HUDManager.instance.m_gameplayUI.SetActive(false);
        GameManager.isPlaying = false;

        StartCoroutine(DeathCam());
        StartCoroutine(HUDManager.ScrambleCamera(HUDManager.instance.m_minimapCamera));
    }

    private void OnTakeDamage()
    {
        HUDManager.SetVignetteAlpha(1);
        HUDManager.instance.m_HPBar.fillAmount = (float)m_HP.m_current / m_HP.m_max;
    }

    public void HandleReload()
    {
        if (!m_isReloading) { return; }

        m_reloadTimer = Mathf.Max(0, m_reloadTimer - Time.deltaTime);

        if (m_reloadTimer <= 0)
        {
            m_mainWeapon.Reload();
            m_isReloading = false;
        }
        else
        {
            m_mainWeapon.Reloading((m_mainWeapon.m_reloadTime - m_reloadTimer) / m_mainWeapon.m_reloadTime);
        }
    }

    public void Reload()
    {
        if (m_mainWeapon == null ||
            m_isReloading ||
            !m_mainWeapon.m_ammo.AllowReload()) { return; }

        m_isReloading = true;
        m_reloadTimer = m_mainWeapon.m_reloadTime;
    }

    public void HandleWeaponPickup()
    {
        //Manual weapon pickup
        //Debug.DrawRay(m_camera.transform.position, m_camera.transform.forward * m_pickupRadius, Color.green);
        if (Input.GetKeyDown(KeyCode.E) && !m_isReloading)
        {
            RaycastHit info;
            Physics.Raycast(m_camera.transform.position, m_camera.transform.forward, out info, m_pickupRadius, m_raycastMask);
            if (info.collider == null) { return; }

            Weapon w = info.collider.GetComponent<Weapon>();
            if (w == null) { return; }

            PickupWeapon(w);
        }

        //Auto pickups
        Collider[] c = Physics.OverlapSphere(transform.position, m_pickupRadius, LayerMask.GetMask("Pickup")); //!!!INEFFICIENT: stop getting same variable each frame

        for (int i = 0; i < c.Length; i++)
        {
            c[i].GetComponent<Pickup>().OnPickup();
        }
    }

    //!!!INCOMPLETE: can only add to one anchor
    public void PickupWeapon(Weapon a_weapon)
    {
        //Search for empty weapon slot
        for (int i = 0; i < m_weaponSlots.Length; i++)
        {
            if (m_weaponSlots[i] == null)
            { m_weaponIndex = i; break; }
        }

        //Try to drop current weapon, if any
        if (m_weaponIndex != 0)
        {
            DropWeapon();
        }
        else
        {
            return;
        }

        ++m_weaponCount;

        a_weapon.Bind(this);

        //Place in anchor
        a_weapon.transform.position = m_weaponAnchors[0].transform.position;
        transform.rotation = transform.rotation;
        a_weapon.transform.parent = m_weaponAnchors[0];

        //Place in weaponSlot
        m_weaponSlots[m_weaponIndex] = a_weapon;
        m_mainWeapon = a_weapon;

        //Put away current weapon
        SetWeapon(m_weaponIndex);
    }

    
    public void AutoPickupWeapon(Weapon a_weapon)
    {
        //If slots full,exit
        if (m_weaponCount >= m_weaponSlots.Length) { return; }
        ++m_weaponCount;

        //Search for empty weapon slot
        for (int i = 0; i < m_weaponSlots.Length; i++)
        {
            if (m_weaponSlots[i] == null)
            { m_weaponIndex = i; break; }
        }

        //Remove rigidbody, if any
        Rigidbody r = a_weapon.GetComponent<Rigidbody>();
        { if (r != null) { Destroy(r); } }

        //Remove Despawn script, if any
        Despawn d = a_weapon.GetComponent<Despawn>();
        { if (d != null) { Destroy(d); } }

        //Disable collider/s
        Collider[] c = a_weapon.GetComponents<Collider>();
        {
            for (int i = 0; i < c.Length; i++)
            { c[i].enabled = false; }
        }

        a_weapon.Bind(this);

        //Place in anchor
        a_weapon.transform.position = m_weaponAnchors[0].transform.position;
        transform.rotation = transform.rotation;
        a_weapon.transform.parent = m_weaponAnchors[0];

        //Place in weaponSlot
        m_weaponSlots[m_weaponIndex] = a_weapon;
        m_mainWeapon = a_weapon;

        //Put away current weapon
        SetWeapon(m_weaponIndex);
    }
}
