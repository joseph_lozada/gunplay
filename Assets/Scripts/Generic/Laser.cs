﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser : MonoBehaviour
{
    public LayerMask m_mask;
    public LineRenderer m_laserRenderer;
    public ParticleSystem m_onLaserHit;
    public int m_damage;
    public float m_maxRange;

    // Use this for initialization
    void Start ()
    {
        m_onLaserHit.transform.forward = Vector3.up;

    }

    // Update is called once per frame
    void Update ()
    {
        RaycastHit info;
        Physics.Raycast(transform.position, transform.forward, out info, m_maxRange, m_mask);

        m_laserRenderer.SetPosition(0, transform.position);
        if (info.collider != null)
        {
            Health h = info.collider.GetComponent<Health>();
            if (h != null)
            { h.TakeDamage(m_damage); }

            m_laserRenderer.SetPosition(1, info.point);
            m_onLaserHit.Play();
            m_onLaserHit.transform.forward = info.normal;
        }
        else
        {
            m_laserRenderer.SetPosition(1, transform.position + transform.transform.forward * m_maxRange);
            m_onLaserHit.Stop();
        }

        m_onLaserHit.transform.position = m_laserRenderer.GetPosition(1);
    }
}
