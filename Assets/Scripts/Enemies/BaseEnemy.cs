﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Health))]
[RequireComponent(typeof(Rigidbody))]
public class BaseEnemy : MonoBehaviour
{
    public int m_dangerRank;        //difficulty (personal bias may be included)
    public int m_level;             //Alter monster difficulty by level
    public Health m_hp;
    public Rigidbody m_rb;
    public int m_damage;

    public ParticleSystem m_onTakeDamageParticle;
    public ParticleSystem m_onDeathParticle;
    [HideInInspector] public Renderer m_renderer;
    public Color m_baseColor;

    private float m_redRatio;
    private const float m_redRatioGravity = 1.2f;   //!!!HARDCODED

    protected void Initialize()
    {
        m_hp = GetComponent<Health>();
        m_rb = GetComponent<Rigidbody>();
        m_renderer = GetComponent<Renderer>();


        //m_onTakeDamageParticle = Instantiate(m_onTakeDamageParticle, transform.parent);
        m_onDeathParticle = Instantiate(m_onDeathParticle,transform);
        m_onDeathParticle.gameObject.transform.localPosition = Vector3.zero;

        //m_baseColor = Color.green;
        m_hp.OnTakeDamage += OnHitColorHandling;
    }

    protected void HandleColor()
    {
        m_redRatio = Mathf.Max(0, m_redRatio -= Time.deltaTime * m_redRatioGravity);

        float hpRatio = (float)m_hp.m_current / m_hp.m_max;
        m_renderer.material.color = (Time.deltaTime * m_renderer.material.color) + ((1- Time.deltaTime) * (hpRatio * m_baseColor) + (Color.black * (1-hpRatio)));
        m_renderer.material.color = (1-m_redRatio) * m_renderer.material.color + m_redRatio * Color.red ;
    }

    protected void OnHitColorHandling()
    {
        if (m_hp.m_current > 0)
        {
            m_redRatio = 1;
        }
        else
        {
            m_renderer.material.color = Color.white;
        }
    }

    protected virtual IEnumerator Death()
    {
        //m_onDeathParticle.Emit(50);//!!!HARDCODED
        m_onDeathParticle.Play();//!!!HARDCODED
        m_rb.constraints = RigidbodyConstraints.None;
        //m_rb.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;
        enabled = false;
        
        gameObject.layer = LayerMask.NameToLayer("IgnoreGameplay");
        //Destroy(this);

        //yield return new WaitUntil(delegate () { return !m_onDeathParticle.isPlaying; });
        float timer = 1;
        const float duration = 1;//!!!HARDCODED
        yield return new WaitWhile(delegate
        {
            timer -= Time.deltaTime / duration;
            m_renderer.material.color = new Color(timer,timer,timer);
            return (timer>0);
        });

        yield return new WaitForSeconds(1);
        Destroy(gameObject);
    }

    protected void TurnTowardsPlayer(float a_rotationSpeed)
    {
        Vector3 v1 = GameManager.m_player.transform.position;
        Vector3 v2 = transform.position;

        v1.y = 0;
        v2.y = 0;

        float angle = Vector3.SignedAngle(transform.forward, v1 - v2, Vector3.up);
        float rotAngle = Mathf.Min(Mathf.Abs(angle), a_rotationSpeed * Time.deltaTime) * Mathf.Sign(angle);
        transform.Rotate(Vector3.up, rotAngle);
    }

    protected void MoveTowardsPlayerHorizontal(float a_movementSpeed)
    {
        Vector3 dir = GameManager.m_player.transform.position - transform.position;
        dir.y = 0;
        dir = dir.normalized * a_movementSpeed;

        Vector3 xz = m_rb.velocity;
        xz.y = 0;
        if (xz.magnitude < a_movementSpeed)
        {
            m_rb.AddForce(new Vector3(dir.x, m_rb.velocity.y, dir.z) * Time.deltaTime, ForceMode.VelocityChange);
        }
    }

    protected void ResetAlignment()
    {
        if (Vector3.Angle(transform.up, Vector3.up) > 1)
        {
            Vector3 dir = transform.forward;
            dir.y = 0;
            transform.LookAt(transform.position + dir.normalized);

            //Remove rotation on everything but the y
            Vector3 rot = m_rb.angularVelocity;
            rot.x = 0;
            rot.z = 0;
            m_rb.angularVelocity = rot;
        }
    }
}
