﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyerEnemy : BaseEnemy
{
    public enum aiState
    {
        Seeking,
        Attacking,
        Stationary,
        Stuck,
        ENUM_END
    }

    public float m_accelSpeed;
    public float m_maxSpeed;
    public float m_targetHeightDiff;    //Target height difference between self and the player
    public float m_aggroDist;           //Distance which triggers aggressive behaviour
    public float m_lockOnTIme;
    public float m_diveSpeed;
    public Transform m_nose;           //Frontmost point on body
    private float m_turnPercentage;
    private Vector3 m_targetPos;
    private float m_flightTimer;

    aiState state;

	// Use this for initialization
	void Start ()
    {
        Initialize();	
	}
	
	// Update is called once per frame
	void Update ()
    {
        switch (state)
        {
            case aiState.Seeking:
                Seeking();
                break;
            case aiState.Attacking:
                Attacking();
                break;
            default:
                break;
        }
	}

    void SwitchState(aiState a_state)
    {
        state = a_state;

        switch (state)
        {
            case aiState.Seeking:
                break;
            case aiState.Attacking:
                m_turnPercentage = 0;
                break;
            case aiState.Stuck:
                StartCoroutine(Stuck());
                break;
            case aiState.Stationary:
                StartCoroutine(Stationary());
                break;
            default:
                break;
        }
    }

    void Seeking()
    {
        Vector3 dir;

        dir = GameManager.m_player.transform.position - transform.position;
        dir.y = (GameManager.m_player.transform.position.y+m_targetHeightDiff) - transform.position.y;

        m_rb.velocity += dir.normalized * m_accelSpeed * Time.deltaTime;
        m_rb.velocity = Mathf.Min(m_rb.velocity.magnitude, m_maxSpeed) * dir.normalized;   //clamp speed

        //check distance for attack
        dir.y = 0;
        if (dir.magnitude <= m_aggroDist)
        { SwitchState(aiState.Attacking); }
    }

    void Attacking()
    {
        Vector3 dir;

        if (m_flightTimer >= 0)
        {
            m_rb.velocity = transform.forward * m_diveSpeed;
            dir = m_nose.position - transform.position;

            RaycastHit info;
            Physics.Raycast(transform.position, dir, out info, dir.magnitude);
            Debug.DrawRay(transform.position, m_nose.position - transform.position, Color.red);

            if (info.collider != null)
            {
                Debug.Log(info.collider.name);
                Health h = info.collider.GetComponent<Health>();
                if (h != null)
                {
                    h.TakeDamage(m_damage);
                }

                m_rb.velocity = -m_rb.velocity * 0.5f;
                SwitchState(aiState.Stationary);
            }

            m_flightTimer -= Time.deltaTime;
            if (m_flightTimer <= 0)
            { SwitchState(aiState.Seeking); }
        }
        else
        {
            dir = GameManager.m_player.transform.position - transform.position;
            Vector3 cross = Vector3.Cross(transform.forward, dir);
            transform.Rotate(cross, Vector3.SignedAngle(transform.forward, dir, cross) * m_turnPercentage, Space.World);

            m_rb.velocity = new Vector3(m_rb.velocity.x * 0.8f, 0, m_rb.velocity.y * 0.8f);
            if (m_lockOnTIme != 0)
            {
                m_turnPercentage = Mathf.Min(1, m_turnPercentage + Time.deltaTime / m_lockOnTIme);
            }
            else
            {
                m_turnPercentage = 1;
            }

            if (m_turnPercentage >= 1)
            {
                m_flightTimer = 3;//!!!HARDCODED
            }
        }
    }

    IEnumerator Stuck()
    {
        Destroy(m_rb);
        Destroy(GetComponent<Collider>());
        yield return new WaitForSeconds(3);//!!!HARDCODED
        Destroy(gameObject);
    }

    IEnumerator Stationary()
    {
        yield return new WaitForSeconds(3);//!!!HARDCODED
        SwitchState(aiState.Seeking);
    }
}
