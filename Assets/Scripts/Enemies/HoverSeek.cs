﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoverSeek : BaseEnemy
{
    public LayerMask m_avoidMask;           //Things to avoid
    //public Transform m_laserSource;
    public Transform m_checkPos;
    public float m_movementSpeed;
    public float m_targetAltitude;

    // Use this for initialization
    void Start()
    {
        Initialize();
        m_hp.OnDeath += delegate ()
        {
            StartCoroutine(Death());
            //m_laserRenderer.enabled = false;
            //m_onLaserHit.gameObject.SetActive(false);
            m_rb.useGravity = true;
        };

        //m_laserRenderer = Instantiate(m_laserRenderer.gameObject, transform).GetComponent<LineRenderer>();

        if (WorldManager.instance != null)
        {
            m_targetAltitude = WorldManager.instance.m_gameworldDimensions.y * 0.5f + 5;
        }
    }
	
	// Update is called once per frame
	void Update ()
    {
        HandleColor();
        AI();
	}

    private void AI()
    {
        TurnTowardsPlayer(180);//!!!HARDCODED
        HandleAltitude();
        MoveTowardsPlayerHorizontal(m_movementSpeed);
        ResetAlignment();
        //UseLaser();
    }

    private void HandleAltitude()
    {
        RaycastHit info;
        
        //Raycast forward
        Debug.DrawRay(m_checkPos.position, transform.forward * 5, Color.red);
        Physics.Raycast(m_checkPos.position, transform.forward, out info, 5, m_avoidMask);//!!!HARDCODED
        if (info.collider != null)
        {
            //m_rb.AddForce(Vector3.up * m_movementSpeed * Time.deltaTime, ForceMode.VelocityChange);
            Vector3 v = m_rb.velocity;
            v.y = m_movementSpeed;
            m_rb.velocity = v;
        }
        else
        {


            //Raycast down
            Physics.Raycast(m_checkPos.position, Vector3.down, out info, float.PositiveInfinity, m_avoidMask);
            if (info.distance < 5)//!!!HARDCODED
            {
                Vector3 v = m_rb.velocity;
                v.y = m_movementSpeed;
                m_rb.velocity = v;
            }
            else
            {
                Vector3 v = m_rb.velocity;
                v.y = m_targetAltitude - transform.position.y;
                m_rb.velocity = v;
            }
        }

        //m_rb.AddForce(Vector3.up * -(m_rb.velocity.y) * Time.deltaTime, ForceMode.VelocityChange);
    }

    //private void UseLaser()
    //{

    //}
}
