﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeekerEnemy : BaseEnemy
{
    public float m_movementSpeed;
    public float m_rotationSpeed;

    // Use this for initialization
    void Start()
    {
        Initialize();

        m_hp.OnDeath += delegate ()
        {
            StartCoroutine(Death());
        };
    }

    // Update is called once per frame
    void Update()
    {
        //if (!GameManager.isPlaying) { enabled = false; }
        HandleColor();
        AI();
    }

    private void AI()
    {
        MoveTowardsPlayerHorizontal(m_movementSpeed);
        TurnTowardsPlayer(m_rotationSpeed);
    }

    private void OnCollisionStay(Collision collision)
    {
        Player p = collision.collider.GetComponent<Player>();
        if (p != null)
        {
            p.m_HP.TakeDamage(m_damage);
        }
    }
}
