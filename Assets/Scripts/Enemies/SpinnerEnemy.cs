﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpinnerEnemy : SeekerEnemy
{
    private int m_turnDir;              //Turn direction. Takes -1 or 1. anything else might cause unexpected behaviour.
    public float m_rotationAccel;
    public float m_maxRotationSpeed;
    //public BoxCollider[] m_hurtBoxes;      //No generic collision check in unity. use boxes.
    //public LayerMask m_HurtboxMask;

    // Use this for initialization
    void Start ()
    {
        Initialize();

        m_turnDir = Random.Range(0, 2) * 2 - 1;
        
        m_hp.OnDeath += delegate ()
        {
            m_rb.AddForce(Vector3.up * 3, ForceMode.VelocityChange);
            m_rb.AddTorque(new Vector3(0,m_rotationSpeed * m_turnDir,0),ForceMode.VelocityChange);
            StartCoroutine(Death());
        };

        //m_hp.OnTakeDamage += OnTakeDamage;
    }
	
	// Update is called once per frame
	void Update ()
    {
        //if (!GameManager.isPlaying) { enabled = false; }
        HandleColor();
        AI();
        ResetAlignment();
        //CheckColliders();
    }

    private void AI()
    {
        MoveTowardsPlayerHorizontal(m_movementSpeed);

        m_rotationSpeed = Mathf.Min(m_maxRotationSpeed, m_rotationSpeed + m_rotationAccel * Time.deltaTime);
        transform.Rotate(Vector3.up, m_rotationSpeed * m_turnDir * Time.deltaTime);
    }

    //private void OnTakeDamage()
    //{
    //    const float damping = 10;
    //    m_rotationSpeed = Mathf.Max(0, m_rotationSpeed - damping);
    //}

    //private void CheckColliders()
    //{
    //    for(int i =0; i < m_hurtBoxes.Length; i++)
    //    {
    //        Collider[] col = Physics.OverlapBox(m_hurtBoxes[i].transform.position, m_hurtBoxes[i].size * 0.5f, m_hurtBoxes[i].gameObject.transform.rotation, m_HurtboxMask);
    //        for (int j = 0; j < col.Length; j++)
    //        {
    //            col[j].GetComponent<Health>().TakeDamage(m_damage);
    //        }
    //    }
    //}

    private void OnCollisionEnter(Collision collision)
    {
        Player p = GetComponent<Player>();
        if (p != null)
        {
            p.m_HP.TakeDamage(m_damage);
        }
    }
}
