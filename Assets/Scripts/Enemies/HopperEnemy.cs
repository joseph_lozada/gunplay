﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HopperEnemy : BaseEnemy
{
    //enum AiState
    //{
    //}
    public LayerMask m_jumpCheckMask;
    public float m_gravity = 10;
    public float m_groundCheckOffset;
    public float m_groundCheckLength;
    public float m_jumpForce;
    public float m_jumpCooldown;
    private float m_jumpCooldownTimer;
    public float m_movementSpeed;

	// Use this for initialization
	void Start ()
    {
        Initialize();

        m_rb.useGravity = false;
        m_jumpCooldownTimer = Random.Range(0.0f, m_jumpCooldown);

        m_hp.OnDeath += delegate ()
          {
              StartCoroutine(Death());
              m_rb.useGravity = true;
          };
	}
	
	// Update is called once per frame
	void Update ()
    {
        //if (!GameManager.isPlaying) { enabled = false; }
        HandleColor();
        AI();

        m_rb.AddForce(Vector3.down * m_gravity * Time.deltaTime, ForceMode.VelocityChange);
	}

    private void AI()
    {
        TurnTowardsPlayer(180);//!!!HARDCODED

        if (Physics.Raycast(transform.position + Vector3.down * m_groundCheckOffset, Vector3.down, m_groundCheckLength, m_jumpCheckMask))
        {
            m_jumpCooldownTimer = Mathf.Max(0, m_jumpCooldownTimer - Time.deltaTime);

            if (m_jumpCooldownTimer <= 0)
            {
                m_jumpCooldownTimer = m_jumpCooldown;
                m_rb.velocity = new Vector3(m_rb.velocity.x, m_jumpForce, m_rb.velocity.z);

                Vector3 dir = GameManager.m_player.transform.position - transform.position;
                dir.y = 0;
                dir = dir.normalized * Mathf.Min(dir.magnitude + m_movementSpeed * 0.2f, m_movementSpeed);
                m_rb.velocity = new Vector3(dir.x, m_rb.velocity.y, dir.z);
            }
        }
        //else
        //{
        //    //Debug.DrawRay(transform.position + Vector3.down * m_groundCheckOffset, Vector3.down * m_groundCheckLength);
        //    //m_rb.AddForce(dir,ForceMode.VelocityChange);
        //}
    }

    private void OnCollisionStay(Collision collision)
    {
        Player p = collision.collider.GetComponent<Player>();
        if (p != null)
        {
            p.m_HP.TakeDamage(m_damage);
        }
    }
}
